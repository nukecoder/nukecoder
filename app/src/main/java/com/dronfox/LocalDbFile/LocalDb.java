package com.dronfox.LocalDbFile;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.dronfox.tappgate.RetrofitInterface;
import com.dronfox.tappgate.getterSetterClass;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Pardeep on 9/19/2015.
 */
public class LocalDb
{
Context ctx;
    ArrayList<String> listUserName;
    ArrayList<String> listExp;
    ArrayList<String> listAvail;
    ArrayList<String> listPath;
    ArrayList<String> listProdName;
    ArrayList<String> listProdPrice;
    ArrayList<String> listProdCurrency;
    ArrayList<String> listProdAvail;
    ArrayList<String> listProdCategory;
    ArrayList<String> listProdSubCat;
    ArrayList<String> listProdDesc;
    ArrayList<String> listProdTimer;
    ArrayList<String> listProdUserId;
    ArrayList<String> listProdServiceId;
    ArrayList<String> listpics;

    RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;

    SQLiteDatabase db;
   public LocalDb(Context c)
    {
        ctx=c;
        db=ctx.openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
    restAdapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com/").build();
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        retrofitInterface=restAdapter.create(RetrofitInterface.class);

    CreateOnDemandTable();
        CreateLocalInfoTable();
        CreatePostedServiceTable();
        CreateRegularDemand();

    }



    public void CreateLocalInfoTable() {
        String createLocalDb = "CREATE TABLE IF NOT EXISTS LocalDb(Name VARCHAR,Experience VARCHAR,Available VARCHAR,profilePicPath VARCHAR,userId VARCHAR)";
        db.execSQL(createLocalDb);

    }
    public void CreatePostedServiceTable() {
        String postedItem="CREATE TABLE IF NOT EXISTS PostedItemTable(serviceName VARCHAR,servicePrice VARCHAR,ServiceCurrency VARCHAR,iteAvaialable VARCHAR,itemCategory VARCHAR,itemSubCategory VARCHAR,itemDescription VARCHAR,itemPostTimer VARCHAR,itemUserId VARCHAR,itemServiceId VARCHAR,itemPIC varchar)";
        db.execSQL(postedItem);
    }
    public void CreateOnDemandTable()
    {
        String CreateOnDemand="CREATE TABLE IF NOT EXISTS OnDemand(demand_ServiceName VARCHAR,demandTotal VARCHAR)";
        db.execSQL(CreateOnDemand);
    }


public void CreateRegularDemand() {
    String create="CREATE TABLE IF NOT EXISTS RegularDemand(ServiceSubCategory VARCHAR,ServiceName Varchar,ServiceProviderName Varchar,ServiceCity Varchar,ServicePrice Varchar,ServiceTimer Varchar,ServiceDays Varchar,serviceUserId Varchar,Type VARCHAR)";
    db.execSQL(create);
}

public void InsertRegularDemand(String SubCategory,String serviceName,String ProviderName,String serviceCity,String servicePrice,String serviceTimer,String serviceDays,String serviceUserId,String type)
{
    String insert="INSERT INTO RegularDemand VALUES('"+SubCategory+"','"+serviceName+"','"+ProviderName+"','"+serviceCity+"','"+servicePrice+"','"+serviceTimer+"','"+serviceDays+"','"+serviceUserId+"','"+type+"')";
    db.execSQL(insert);
}



    public void SelectRegularDemand(String userId,
            ArrayList<String> subcatename,
                                    ArrayList<String> serviceName,
                                    ArrayList<String> serviceProviderName,
                                    ArrayList<String> serviceCity,
                                    ArrayList<String> servicePrice,
                                    ArrayList<String> serviceTimer,
                                    ArrayList<String> serviceDays,
                                    ArrayList<String> serviceUserId,

    String type
    )
    {

        subcatename.clear();
        serviceName.clear();
        serviceProviderName.clear();
        serviceCity.clear();
        servicePrice.clear();
        serviceTimer.clear();
        serviceDays.clear();
        serviceUserId.clear();



        String select="SELECT * FROM RegularDemand WHERE Type='"+type+"'";
        Cursor crs=db.rawQuery(select,null);
        if(crs.getCount() < 1)
        {



        }
        else
        {
            crs.moveToFirst();
            do
            {
                String getServiceCat=crs.getString(crs.getColumnIndex("ServiceSubCategory"));

                String getServiceName=crs.getString(crs.getColumnIndex("ServiceName"));
                String getServiceProvider=crs.getString(crs.getColumnIndex("ServiceProviderName"));
                String getServiceCity=crs.getString(crs.getColumnIndex("ServiceCity"));
                String getServicePrice=crs.getString(crs.getColumnIndex("ServicePrice"));
                String getServiceTimer=crs.getString(crs.getColumnIndex("ServiceTimer"));
                String getServiceDays=crs.getString(crs.getColumnIndex("ServiceDays"));
                String getServiceUserId=crs.getString(crs.getColumnIndex("serviceUserId"));


                subcatename.add(getServiceCat);
                serviceName.add(getServiceName);
                serviceProviderName.add(getServiceProvider);
                serviceCity.add(getServiceCity);
                servicePrice.add(getServicePrice);
                serviceTimer.add(getServiceTimer);
                serviceDays.add(getServiceDays);
                serviceUserId.add(getServiceUserId);



            }
            while (crs.moveToNext());
        }
    }





    public void INsertIntoLocalInfo(String Name,String Experience,String Available,String ProfilePic,String userId)
     {
String insert="INSERT INTO LocalDb(Name,Experience,Available,profilePicPath,userId)VALUES('"+Name+"','"+Experience+"','"+Available+"','"+ProfilePic+"','"+userId+"')";
   db.execSQL(insert);
     }


    public void updateLocalInfoDb(String userId,String colName,String ValuesCount,String serverSideValue,File file)
    {



        String update="UPDATE LocalDb SET "+colName+"= '"+ValuesCount+"'"+ " WHERE userId='"+userId+"'";
        db.execSQL(update);

            retrofitInterface.updateProfile(userId, colName, serverSideValue, new Callback<String>() {
                @Override
                public void success(String s, Response response) {
                    Log.e("Data Modified", "" + s);
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Log.e("Data Modified", "" + retrofitError.getMessage());
                }
            });

TypedFile typedFile = new TypedFile("application/octate-stream", file);

        retrofitInterface.insertImage(typedFile, new Callback<String>() {
            @Override
            public void success(String s, Response response) {

            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });

    }
    public void insertInPostedService(String setName,String setPrice, String setCurrency,String setItemAvailable,String setItemCategory,String setItemSubCategory,String setItemDesc,String setItemPostTimer,String setItemUserId,String setItemServiceId,String itemPIC)
    {

        String insertProduct="Insert INTO PostedItemTable VALUES('"+setName+"','"+setPrice+"','"+setCurrency+"','"+setItemAvailable+"','"+setItemCategory+"','"+setItemSubCategory+"','"+setItemDesc+"','"+setItemPostTimer+"','"+setItemUserId+"','"+setItemServiceId+"','"+itemPIC+"')";
        db.execSQL(insertProduct);


       // getServiceName, getServicePrice, getCurrency, getItemAvailability, getCategory, getSubCatgory, getDescription, getImagePath, getPosttimer, getUserId, getServiceId, new Callback<String>() {




      }

    public void SelectPersonalInfo(ArrayList<String>listName,ArrayList<String>listExp,ArrayList<String>listAvail,ArrayList<String>listPath,ArrayList<String> listProdUserId)
    {
        listName.clear();
        listAvail.clear();
        listExp.clear();
        listPath.clear();
        listProdUserId.clear();
        String select="SELECT * FROM LocalDb";
        Cursor crs=db.rawQuery(select,null);
        if(crs.getCount() <1)
        {
            Toast.makeText(ctx,"No Values Found",Toast.LENGTH_SHORT).show();
        }
        else

        {
        crs.moveToFirst();
            do
            {


                String getName=crs.getString(crs.getColumnIndex("Name"));
                String getExp=crs.getString(crs.getColumnIndex("Experience"));
                String getAvailable=crs.getString(crs.getColumnIndex("Available"));
                String ProfilePicPath=crs.getString(crs.getColumnIndex("profilePicPath"));
                String userId=crs.getString(crs.getColumnIndex("userId"));
                listName.add(getName);
                listExp.add(getExp);
                listAvail.add(getAvailable);
                listPath.add(ProfilePicPath);
                listProdUserId.add(userId);
            }
            while (crs.moveToNext());



        }





    }





public void MyPostedService(final ArrayList<String> listName, final ArrayList<String> listprice, final ArrayList<String> listcurrency, final ArrayList<String> listavail, final ArrayList<String> listcategory, final ArrayList<String> listsubcategory, final ArrayList<String> listDesc, final ArrayList<String> listposttimer, final String listuserid, final ArrayList<String> listserviceid, final ArrayList<String> listPic)
{
    listName.clear();
    listprice.clear();
    listcurrency.clear();
    listavail.clear();
    listcategory.clear();
    listsubcategory.clear();
    listDesc.clear();
    listposttimer.clear();

    listserviceid.clear();
    listPic.clear();
String select="SELECT * FROM PostedItemTable";
    Cursor crs=db.rawQuery(select,null);
    if(crs.getCount() < 1)
    {

        retrofitInterface.selectedPostService(listuserid, new Callback<List<getterSetterClass>>() {
            @Override
            public void success(List<getterSetterClass> getterSetterClass, Response response) {
                for(int i=0;i<getterSetterClass.size();i++)
                {


                    Log.e("Logs",""+getterSetterClass.size());



                    String setName=getterSetterClass.get(i).getServicePicPath();
                    String setPrice=getterSetterClass.get(i).getServicePrice();
                    String setCurrency=getterSetterClass.get(i).getServiceCurrency();
                    String setItemAvailable=getterSetterClass.get(i).getServiceAvailable();
                    String setItemCategory=getterSetterClass.get(i).getServiceCategory();
                    String setItemSubCat=getterSetterClass.get(i).getServiceSubCategory();
                    String serviceName=getterSetterClass.get(i).getServiceName();
                   String setItemDesc=getterSetterClass.get(i).getServiceDesc();
                    String getUserId=getterSetterClass.get(i).getUserId();
                        String setItemPostTimer=getterSetterClass.get(i).getServicePostTimer();
                   String setItemServiceId=getterSetterClass.get(i).getServiceId();
                    String itemPIC=getterSetterClass.get(i).getServicePicPath();

                   //Log.e("Logs",""+setItemPostTimer);

                    Log.e("Logs",""+setName+"\n"+setPrice+"\n"+setCurrency+"\n"+setItemAvailable+"\n"+setItemCategory+"\n"+setItemDesc+"\n"+getUserId+"\n"+itemPIC);
                    //(serviceName VARCHAR,servicePrice VARCHAR,ServiceCurrency VARCHAR,iteAvaialable VARCHAR,itemCategory VARCHAR,itemSubCategory VARCHAR,itemDescription VARCHAR,itemPostTimer VARCHAR,itemUserId VARCHAR,itemServiceId VARCHAR,itemPIC varchar)";
insertInPostedService(serviceName,setPrice,setCurrency,setItemAvailable,setItemCategory,setItemSubCat,setItemDesc,setItemPostTimer,getUserId,setItemServiceId,itemPIC);

              //      MyPostedService(listName,listprice, listcurrency,listavail,listcategory,listsubcategory,listDesc,listposttimer,listuserid,listserviceid,listPic);


                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });





    }
    else
    {
        crs.moveToFirst();
        do
        {




           // "serviceName VARCHAR,servicePrice VARCHAR,ServiceCurrency VARCHAR,iteAvaialable VARCHAR,itemCategory VARCHAR,itemSubCategory VARCHAR,itemDescription VARCHAR,itemPostTimer VARCHAR,itemUserId VARCHAR,itemServiceId VARCHAR)";


            String getServiceName=crs.getString(crs.getColumnIndex("serviceName"));
            String getservicePrice=crs.getString(crs.getColumnIndex("servicePrice"));
            String getServiceCurrency=crs.getString(crs.getColumnIndex("ServiceCurrency"));
            String getiteAvaialable=crs.getString(crs.getColumnIndex("iteAvaialable"));
            String getitemCategory=crs.getString(crs.getColumnIndex("itemCategory"));
            String getitemSubCategory=crs.getString(crs.getColumnIndex("itemSubCategory"));
            String getitemDescription=crs.getString(crs.getColumnIndex("itemDescription"));
            String getitemPostTimer=crs.getString(crs.getColumnIndex("itemPostTimer"));
            String getitemUserId=crs.getString(crs.getColumnIndex("itemUserId"));
            String getitemServiceId=crs.getString(crs.getColumnIndex("itemServiceId"));
            String getItemPic=crs.getString(crs.getColumnIndex("itemPIC"));

            listName.add(getServiceName);
            listprice.add(getservicePrice);
            listcurrency.add(getServiceCurrency);
            listavail.add(getiteAvaialable);
            listcategory.add(getitemCategory);
            listsubcategory.add(getitemSubCategory);
            listDesc.add(getitemDescription);
            listposttimer.add(getitemPostTimer);

            listserviceid.add(getitemServiceId);
            listPic.add(getItemPic);

        }
        while (crs.moveToNext());
    }


}
public int getSize(int size)
{
    String select="SELECT * FROM PostedItemTable";
    Cursor cursor=db.rawQuery(select,null);
    size=cursor.getCount();

return size;
}






   public int getSizeOndamand(int size)
   {
       String select="SELECT * FROM OnDemand";
       Cursor crs=db.rawQuery(select,null);

     size=crs.getCount();





       return size;
   }
}
