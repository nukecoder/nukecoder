package com.dronfox.tappgate;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Pardeep on 9/17/2015.
 */
public class Activity_RegularService extends ActionBarActivity
{

LinearLayout regular_Linearlayout;
    RecyclerView regularRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_regularservice);
    regular_Linearlayout=(LinearLayout)findViewById(R.id.RegularLinearLayout);
        regularRecyclerView=(RecyclerView)findViewById(R.id.regular_Recycler);
        regularRecyclerView.setVisibility(View.GONE);


    }
}
