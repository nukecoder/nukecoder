package com.dronfox.tappgate;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

public interface RetrofitInterface

{
    @Multipart
    @POST("/Omiindo/tappgate/upload.php")
    void insertImage(@Part("image") TypedFile typefile, Callback<String> ca);


    @FormUrlEncoded
    @POST("/Omiindo/tappgate/Registration.php")
    void insertData(@Field("contactnum") String getContact, @Field("deviceid") String getDeviceId, @Field("city") String getCity, @Field("state") String getState, @Field("userId") String getUserId, @Field("country") String getCountry, @Field("gcmId") String getGcmId, Callback<String> ca);


    @FormUrlEncoded
    @POST("/Omiindo/tappgate/UserInfo.php")
    void insertUserInfo(@Field("Name") String putName, @Field("Experience") String putExperience, @Field("Availability") String putAvail, @Field("Rating") String putRating, @Field("Totalpost") String putTotalpost, @Field("userId") String putUserId, @Field("onlineStatus") String serviceId, @Field("profilePicPath") String getProfilePic, Callback<String> putUserInformation);


    @FormUrlEncoded
    @POST("/Omiindo/tappgate/AddItem.php")
    void addItems(@Field("serviceName") String getServiceName, @Field("servicePrice") String getServicePrice, @Field("serviceCurrency") String getServiceCurrency, @Field("serviceAvailable") String getServiceAvailable, @Field("serviceCategory") String getCategory, @Field("serviceSubCategory") String getSubCategory, @Field("serviceDesc") String getServiceDesc, @Field("servicePicPath") String getUserPid, @Field("servicePostTimer") String getPublishTimer, @Field("userId") String getImagePath, @Field("serviceId") String getServiceId, Callback<String> add);


    @GET("/Omiindo/tappgate/allPost.php")
    void getTotalPost(Callback<String> callback);


    @GET("/Omiindo/tappgate/DisplayDataApi.php")
    void fetchProductDetails(Callback<List<getterSetterClass>> getterSetterClassCallback);

    @FormUrlEncoded
    @POST("/Omiindo/tappgate/DisplayItemConditionBased.php")
    void fetchProductDetailsCondition(@Field("serviceId") String service, Callback<List<getterSetterClass>> getterSetterClassCallback);




//
//    @FormUrlEncoded
//    @POST("/Omiindo/tappgate/MyPostedServices.php")
//    void insertMyService(@Field("serviceName") String serviceName,@Field("servicePrice") String servicePrice,
//                         @Field("ServiceCurrency") String ServiceCurrency,@Field("itemAvaialable") String itemAvaialable,
//                         @Field("itemCategory") String itemCategory,@Field("itemSubCategory") String itemSubCategory,
//                         @Field("itemDescription") String itemDescription,
//                         @Field("itemPostTimer") String itemPostTimer,@Field("userId") String userId,
//                         @Field("itemServiceId") String serviceName,@Field("itemPIC") String serviceName,
//                         );
//




    @FormUrlEncoded
    @POST("/Omiindo/tappgate/selectedPostedService.php")
    void selectedPostService(@Field("userId") String userId, Callback<List<getterSetterClass>> getterSetterClassCallback);



    @FormUrlEncoded
    @POST("/Omiindo/tappgate/RegistrationChecking.php")
    void checkLogin(@Field("lastdigit") String IMEI,Callback<String> chck);




    @FormUrlEncoded
    @POST("/Omiindo/tappgate/UpdateUserProfile.php")
    void updateProfile(@Field("lastdigit") String userId,@Field("ColumnName") String colName,@Field("ColumnValue") String colValue,Callback<String> chck);





}