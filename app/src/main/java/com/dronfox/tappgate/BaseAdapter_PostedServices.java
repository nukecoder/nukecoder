package com.dronfox.tappgate;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TableRow;
import android.widget.TextView;

import com.dronfox.Circular.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Pardeep on 9/16/2015.
 */
public class BaseAdapter_PostedServices extends RecyclerView.Adapter<BaseAdapter_PostedServices.ViewHolder>
{

    PopupWindow popupWindow;
    ArrayList<String> serviceName;
    ArrayList<String> servicDesc;
    ArrayList<String> serviceAvail;
    ArrayList<String> serviceProfilePic;
    ArrayList<String> servicePrice;
    ArrayList<String> serviceCurrency;
    ArrayList<String> serviceId;


    Context ctx;
    BaseAdapter_PostedServices(Context ct,ArrayList<String> Name,ArrayList<String> Desc,ArrayList<String> Avail,ArrayList<String> pic,ArrayList<String> price,ArrayList<String> currency,ArrayList<String> listId)
    {
        ctx=ct;
        serviceName=Name;
        servicDesc=Desc;
        serviceAvail=Avail;
        serviceProfilePic=pic;
        servicePrice=price;
        serviceCurrency=currency;
        serviceId=listId;
    }
    @Override
    public BaseAdapter_PostedServices.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View  inflater=LayoutInflater.from(parent.getContext()).inflate(R.layout.baseadapter_mypostedservices,parent,false);

        ViewHolder vh=new ViewHolder(inflater);

        return vh;
    }

    @Override
    public void onBindViewHolder(final BaseAdapter_PostedServices.ViewHolder holder, final int position) {


        holder.cardEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
                View popupview = layoutInflater.inflate(R.layout.popupbubble, null);
                TableRow change = (TableRow) popupview.findViewById(R.id.change);

                popupWindow = new PopupWindow(popupview, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
              //  popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, 0, 0);
               popupWindow.showAsDropDown(v,50,0);
                change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });


            }
        });




        holder.textPostServic.setText("" + serviceName.get(position));
        holder.textPostDesc.setText("" + servicDesc.get(position));
       holder.textAvail.setText("" + serviceAvail.get(position));
       holder.textPrice.setText("" + servicePrice.get(position) + " " + serviceCurrency.get(position));
        File file=new File(serviceProfilePic.get(position));
        Picasso.with(ctx).load(serviceProfilePic.get(position)).resize(80,80).into(holder.demand_service_Pic);

//
    }

    @Override
    public int getItemCount() {

        return serviceName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {
            CircularImageView demand_service_Pic;

        TextView textPostServic;
        TextView textPostDesc;
        TextView textAvail;
        TextView textPrice;
        CardView cardEdit;

        public ViewHolder(View itemView) {




            super(itemView);

cardEdit=(CardView)itemView.findViewById(R.id.cardEdit);
demand_service_Pic=(CircularImageView)itemView.findViewById(R.id.demand_service_Pic);
            textPostServic=(TextView)itemView.findViewById(R.id.textPostServic);
            textPostDesc=(TextView)itemView.findViewById(R.id.textPostDesc);
            textAvail=(TextView)itemView.findViewById(R.id.textAvail);
            textPrice=(TextView)itemView.findViewById(R.id.textPrice);


        }
    }
}
