package com.dronfox.tappgate;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

class ViewPagerAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<Bitmap>arrayList;
    LayoutInflater mLayoutInflater;
    public ViewPagerAdapter(Context context,ArrayList<Bitmap> bmp) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      arrayList=bmp;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.viewpageradapter_maindashboard, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.pageradapter_Image);
        // imageView.setImageResource(reso[position]);
// 



      imageView.setImageBitmap(arrayList.get(position));
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}