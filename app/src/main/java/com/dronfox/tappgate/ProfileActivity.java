package com.dronfox.tappgate;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dronfox.Circular.CircularImageView;
import com.dronfox.DemandAndRegularFiles.OnDemand_ShowMyServices;
import com.dronfox.DemandAndRegularFiles.Regular_ShowMyServices;
import com.dronfox.LocalDbFile.LocalDb;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Pardeep on 9/15/2015.
 */
public class ProfileActivity extends ActionBarActivity implements View.OnClickListener

{
    int REQUEST_CODE_GALLERY = 0;
    int REQUEST_CODE_CAMERA = 1;
    ImageView edt_Timer;
    ImageView edt_Contact;
    EditText edt_Update;
    EditText edt_newContact;
    Button btnOndemand;
    Button btnOnRegular;
    Button profile_Post;
    ImageView edtValues;
    Button profile_History;
    ArrayList<String> listName;
    ArrayList<String> listAvail;
    ArrayList<String> listExper;
    ArrayList<String> listprofilepic;
    ArrayList<String> listId;
    CircularImageView profilePicEdit;
    LocalDb objLocalDb;
    TextView profileName;
    TextView profileexprnce;
    ImageView imgEdit;
    Extra_Functions functions;
    int size;
    int demandsize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profileactivity);

        functions = new Extra_Functions(this);
        listName = new ArrayList<String>();
        listAvail = new ArrayList<String>();
        listExper = new ArrayList<String>();
        listprofilepic = new ArrayList<String>();
        listId = new ArrayList<String>();
        profilePicEdit = (CircularImageView) findViewById(R.id.profilePicEdit);
        profileName = (TextView) findViewById(R.id.profileName);
        edt_Contact = (ImageView) findViewById(R.id.edt_Contact);
        edt_Timer = (ImageView) findViewById(R.id.edt_Timer);
        btnOndemand = (Button) findViewById(R.id.btnOndemand);
        btnOnRegular = (Button) findViewById(R.id.btnOnRegular);
        profile_History = (Button) findViewById(R.id.profile_History);
        profile_Post = (Button) findViewById(R.id.profile_Post);
        edtValues=(ImageView)findViewById(R.id.edit_Values);
        profileexprnce = (TextView) findViewById(R.id.profileexprnce);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        objLocalDb = new LocalDb(this);
        objLocalDb.CreateLocalInfoTable();
        objLocalDb.CreatePostedServiceTable();
       // objLocalDb.CreateOnDemandTable();;
        objLocalDb.SelectPersonalInfo(listName, listExper, listAvail, listprofilepic, listId);



        profile_Post.setText(""+objLocalDb.getSize(size)+"\n"+"Posted Service");

        Bitmap bmp = BitmapFactory.decodeFile(listprofilepic.get(0));
        profilePicEdit.setImageBitmap(bmp);
        profileName.setText("" + listName.get(0));
        profileexprnce.setText("" + listExper.get(0) + " Year ");
        edt_Contact.setOnClickListener(this);
        edt_Timer.setOnClickListener(this);
        btnOndemand.setOnClickListener(this);
        btnOnRegular.setOnClickListener(this);
        profile_Post.setOnClickListener(this);
        profile_History.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        edtValues.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.edit_Values:








                break;
            case R.id.imgEdit:


                CaptureImageFunction();


                break;
            case R.id.profile_Post:
                Intent iop = new Intent(ProfileActivity.this, Activity_PostedService.class);


                startActivity(iop);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;
            case R.id.profile_History:
                Toast.makeText(ProfileActivity.this, "Working", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnOnRegular:
                Intent regularservice = new Intent(ProfileActivity.this, Regular_ShowMyServices.class);
                regularservice.putExtra("Extra","ok");
                startActivity(regularservice);
                break;
            case R.id.btnOndemand:



                  Intent demandActivity = new Intent(ProfileActivity.this,  OnDemand_ShowMyServices.class);
                demandActivity.putExtra("Extra","ok");

                startActivity(demandActivity);
                  overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);


                break;
            case R.id.edt_Contact:
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                LayoutInflater infl = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View viewInfl = infl.inflate(R.layout.edit_contact_number, null);
                builder.setView(viewInfl);
                builder.setTitle("Update Contact Number");

                builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create();
                builder.show();


                break;
            case R.id.edt_Timer:
                break;
        }
    }


    public void CaptureImageFunction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Product Image");
        builder.setMessage("Please Select a way to pick the Product Image");
        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    Intent gallery = new Intent(Intent.ACTION_PICK);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, REQUEST_CODE_GALLERY);

                } catch (Exception e) {

                }
            }
        });

        builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera, REQUEST_CODE_CAMERA);
                } catch (Exception e) {

                }
            }
        });

        builder.create();
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_CODE_CAMERA && data != null) {

            try {
                Bitmap clicked_image = (Bitmap) data.getExtras().get("data");

                Drawable d = new BitmapDrawable(clicked_image);


                profilePicEdit.setImageBitmap(clicked_image);
                functions.Save_Image_To_sdCard(clicked_image);
                String getImagePathonDisk = functions.filepath;

                File f = new File(getImagePathonDisk);




                objLocalDb.updateLocalInfoDb(listId.get(0), "profilePicPath", "" + functions.filepath,"http://omiindo.com/Omiindo/tappgate/images/" + functions.imageFileName,f) ;
                Toast.makeText(this, "iamge path" + getImagePathonDisk, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == REQUEST_CODE_GALLERY && data != null) {
            try {

                Uri photoUri = data.getData();
                if (photoUri != null) {
                    try {
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String filePath = cursor.getString(columnIndex);
                        cursor.close();

                        Bitmap photo = BitmapFactory.decodeFile(filePath);
                        Drawable d = new BitmapDrawable(photo);

                        profilePicEdit.setImageBitmap(photo);
                        functions.Save_Image_To_sdCard(photo);

                        String getImagePathonDisk = functions.filepath;
                        File f = new File(getImagePathonDisk);
                        objLocalDb.updateLocalInfoDb(listId.get(0), "profilePicPath", "" + functions.filepath,"http://omiindo.com/Omiindo/tappgate/images/" + functions.imageFileName,f) ;

                        Toast.makeText(this, "Image Path  " + getImagePathonDisk, Toast.LENGTH_SHORT).show();
                   } catch (Exception e) {
                        Log.e("error",""+e);
                    }
                }
            }catch (Exception e)
            {
                Log.e("error", "" + e);
        }
    }
}
}