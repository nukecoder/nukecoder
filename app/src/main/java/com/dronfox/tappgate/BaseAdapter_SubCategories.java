package com.dronfox.tappgate;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dronfox.Circular.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Pardeep on 8/30/2015.
 */
public class BaseAdapter_SubCategories  extends RecyclerView.Adapter<BaseAdapter_SubCategories.ViewHolder>{


    Context context;
    ArrayList<String> listName;
    ArrayList<String> listServiceName;
    ArrayList<String> listServiceImagePath;
    ArrayList<String> listRating;
    ArrayList<String> listTimer;
    ArrayList<String> listDesc;
    ArrayList<String> listserviceId;
    ArrayList<String> listItemSubCat;
    public BaseAdapter_SubCategories(Context Ctx,ArrayList<String>listNames,ArrayList<String>listServiceNames,ArrayList<String>listServiceImagePaths,ArrayList<String>listRatings,ArrayList<String>listTimers,ArrayList<String>listDescs,ArrayList<String> Ids,ArrayList<String>subcat)
    {
        context=Ctx;
    listName=listNames;
        listServiceName=listServiceNames;
        listServiceImagePath=listServiceImagePaths;
                listRating=listRatings;
        listTimer=listTimers;
                listDesc=listDescs;
listserviceId=Ids;
        listItemSubCat=subcat;

    }

    @Override
    public BaseAdapter_SubCategories.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View inflaterview= LayoutInflater.from(parent.getContext()).inflate(R.layout.baseadapter_subcategory, parent,false);
        ViewHolder vh=new ViewHolder(inflaterview);

        return vh;
    }

    @Override
    public void onBindViewHolder(BaseAdapter_SubCategories.ViewHolder holder, final int position) {



        Picasso.with(context).load(listServiceImagePath.get(position)).resize(150,150).into(holder.serviceImage);
        holder.textService.setText("" + listServiceName.get(position));
        holder.textProvider.setText("" + listName.get(position));
        holder.textDesc.setText(""+listDesc.get(position));
        holder.ratingBar.setRating(Float.parseFloat("" + listRating.get(position)));
        holder.textSubCat.setText(listItemSubCat.get(position));
        holder.textTimer.setText(""+listTimer.get(position));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Fragment FragementDisplayItem=new Fragment_DisplayItem();
//
//                Bundle bundle =new Bundle();
//                bundle.putString("serviceId",""+listserviceId.get(position));
//
//                FragementDisplayItem.setArguments(bundle);
//
//                FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
//
//                tranc.replace(R.id.content_frame, FragementDisplayItem).addToBackStack(null);
//                tranc.commit();
//                Toast.makeText(context,listserviceId.get(position),Toast.LENGTH_LONG).show();


                Intent iop=new Intent(context,Fragment_DisplayItem.class);
                iop.putExtra("serviceId",""+listserviceId.get(position));
                context.startActivity(iop);


            }
        });
    }

    @Override
    public int getItemCount() {

        return listName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder {

        CardView cardView;
        CircularImageView serviceImage;
        TextView textService;
        TextView textProvider;
        TextView textDesc;
        TextView textSubCat;
        RatingBar ratingBar;
        TextView textTimer;

        public ViewHolder(View itemView)
        {
            super(itemView);

cardView=(CardView)itemView.findViewById(R.id.card_details);
serviceImage=(CircularImageView)itemView.findViewById(R.id.item_image);
            textService=(TextView)itemView.findViewById(R.id.itemName);
            textProvider=(TextView)itemView.findViewById(R.id.item_Provider_Name);
            textDesc=(TextView)itemView.findViewById(R.id.item_Descriptions);
            ratingBar=(RatingBar)itemView.findViewById(R.id.ratingBar1);
            textTimer=(TextView)itemView.findViewById(R.id.item_post_Timer);
            textSubCat=(TextView)itemView.findViewById(R.id.itemSubCategory);
        }
    }

}
