package com.dronfox.tappgate;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dronfox.Circular.CircularImageView;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/16/2015.
 */
public class BaseAdapter_Regular_ServicePeopleList extends RecyclerView.Adapter<BaseAdapter_Regular_ServicePeopleList.ViewHolder>
{

    PopupWindow popupWindow;
    ArrayList<String> serviceName;
    ArrayList<String> serviceUserId;
    ArrayList<String> serviceuserName;
    ArrayList<String> servicePrice;
    ArrayList<String> serviceCurrency;
    ArrayList<String> serviceAvail;
    Context ctx;
    BaseAdapter_Regular_ServicePeopleList(Context ct,ArrayList<String> userId,
                                          ArrayList<String> userName,
                                          ArrayList<String> servicename,
                                          ArrayList<String> serviceprice,
                                          ArrayList<String> servicecurrency,
                                          ArrayList<String> serviceavail)
    {



        ctx=ct;

        serviceName=servicename;
        serviceUserId=userId;
        serviceuserName=userName;
        servicePrice=serviceprice;
        serviceCurrency=servicecurrency;
        serviceAvail=serviceavail;
    }
    @Override
    public BaseAdapter_Regular_ServicePeopleList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View  inflater=LayoutInflater.from(parent.getContext()).inflate(R.layout.baseadapter_regularsetting_showlist,parent,false);

        ViewHolder vh=new ViewHolder(inflater);

        return vh;
    }

    @Override
    public void onBindViewHolder(final BaseAdapter_Regular_ServicePeopleList.ViewHolder holder, final int position) {

holder.regular_textPostServic.setText(""+serviceName.get(position));

        holder.regular_textPostDesc.setText(""+serviceuserName.get(position));

        holder.regular_textAvail.setText(""+serviceAvail.get(position));

        holder.Regular_textPrice.setText(""+servicePrice.get(position) + " "+serviceCurrency.get(position));






//
    }

    @Override
    public int getItemCount() {

        return serviceName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {

CircularImageView regular_customer_Pic;
        TextView regular_textPostServic;
        TextView regular_textPostDesc;
        TextView regular_textAvail;
    TextView Regular_textPrice;
        public ViewHolder(View itemView) {
            super(itemView);
            regular_customer_Pic=(CircularImageView)itemView.findViewById(R.id.regular_customer_Pic);

            regular_textPostServic=(TextView)itemView.findViewById(R.id.regular_textPostServic);

            regular_textPostDesc=(TextView)itemView.findViewById(R.id.regular_textPostDesc);

            regular_textAvail=(TextView)itemView.findViewById(R.id.regular_textAvail);

            Regular_textPrice=(TextView)itemView.findViewById(R.id.Regular_textPrice);
        }
    }
}
