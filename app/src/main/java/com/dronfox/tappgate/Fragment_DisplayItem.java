package com.dronfox.tappgate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.dronfox.Circular.CircularImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.w3c.dom.Text;

/**
 * Created by Pardeep on 8/31/2015.
 */
public class Fragment_DisplayItem extends ActionBarActivity {

    String providerContact;
    TextView itemPrice;
    TextView numCustomer;
    TextView displayProviderName;
    SQLiteDatabase database;
    String profile;
    ImageView MyPic;
    TextView displayContactNumber;
    TextView displayProviderAvailableTime;
    TextView providerDescText;
    TextView totalProducts;
    String getDeviceId;
    ImageView image;
    String getService;
    ActionBar actionbar;
    int postCount;
    CircularImageView actionProfile;
TextView productAvailCityText;
    TextView productTextName;
   int screenWIdth;
    int screenHeight;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionbar = getSupportActionBar();
        actionbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#5F9EA0")));
        setContentView(R.layout.fragment_displayitem);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View viewPrilf = inflater.inflate(R.layout.customactionbar, null);
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setCustomView(viewPrilf);
        actionbar.setDisplayHomeAsUpEnabled(true);
        //actionProfile.setBackgroundResource(R.mipmap.ic_launcher);
        actionbar.setTitle("");
        actionProfile = (CircularImageView) viewPrilf.findViewById(R.id.actionbarProfile);
        actionProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog showProfilePic = new Dialog(Fragment_DisplayItem.this);
                showProfilePic.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                showProfilePic.setContentView(R.layout.dialogprofilepicview);
                MyPic = (ImageView) showProfilePic.findViewById(R.id.myProfilePic);
                Picasso.with(Fragment_DisplayItem.this).load(profile).resize(512, 512).into(MyPic);
                showProfilePic.show();
            }
        });

        database = openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String create = "CREATE TABLE IF NOT EXISTS DisplayApi(Name VARCHAR,contactnum VARCHAR,deviceid VARCHAR,Experience VARCHAR,Availability VARCHAR,Rating VARCHAR,Totalpost VARCHAR,profilePicPath VARCHAR,onlineStatus VARCHAR,serviceName VARCHAR,servicePrice VARCHAR,serviceCurrency VARCHAR,serviceAvailable VARCHAR,serviceCategory VARCHAR,serviceSubCategory VARCHAR,serviceDesc VARCHAR,servicePicPath VARCHAR,servicePostTimer VARCHAR,serviceId VARCHAR,gcmId VARCHAR)";
        database.execSQL(create);

        Display display = getWindowManager().getDefaultDisplay();
        screenWIdth = display.getWidth();
        screenHeight = display.getHeight();

        String serviceId = getIntent().getStringExtra("serviceId");
        numCustomer = (TextView) findViewById(R.id.numCustomer);
        displayProviderName = (TextView) findViewById(R.id.displayProviderName);
        displayContactNumber = (TextView) findViewById(R.id.displayProviderContactNumber);
        itemPrice = (TextView) findViewById(R.id.displayItemPrice);
        productAvailCityText=(TextView)findViewById(R.id.productAvailCityText);
        productTextName=(TextView)findViewById(R.id.productTextName);
        displayProviderAvailableTime = (TextView) findViewById(R.id.displayProviderAvailableTime);
  //      displayTabRow = (TableRow) findViewById(R.id.displayTabRow);
        providerDescText = (TextView) findViewById(R.id.providerDescText);
        totalProducts = (TextView) findViewById(R.id.totalProducts);
        image = (ImageView) findViewById(R.id.image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dig = new Dialog(Fragment_DisplayItem.this);
                dig.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dig.setContentView(R.layout.dialogshowimage);

                ImageView imgServiceImage = (ImageView) dig.findViewById(R.id.imageService);
                Picasso.with(Fragment_DisplayItem.this).load(getService).into(imgServiceImage);

                Button btnDone = (Button) dig.findViewById(R.id.button_Done);
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dig.dismiss();
                    }
                });

                dig.show();
            }
        });



        SelectDetails(serviceId);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menudisplayitem, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
             finish();
                return true;

            case R.id.call:

                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse("tel:" + providerContact));
                startActivity(call);

                break;


        }
        return super.onOptionsItemSelected(item);

    }



    public void SelectDetails(String getServiceId)
    {
        String selectData="SELECT * FROM DisplayApi WHERE serviceId='"+getServiceId+"'";
        Cursor cursor=database.rawQuery(selectData,null);
        if(cursor.getCount() < 1)
        {
            Snackbar.make(findViewById(android.R.id.content),"No Data Found",Snackbar.LENGTH_LONG).show();
        }
        else
        {
            cursor.moveToNext();
            do {
                String providerName=cursor.getString(cursor.getColumnIndex("Name"));
                String Experience=cursor.getString(cursor.getColumnIndex("Experience"));
                String Availability=cursor.getString(cursor.getColumnIndex("Availability"));
                providerContact=cursor.getString(cursor.getColumnIndex("Contactnum"));
                String providerDesc=cursor.getString(cursor.getColumnIndex("serviceDesc"));
                getService  =cursor.getString(cursor.getColumnIndex("servicePicPath"));
                String getPrice=cursor.getString(cursor.getColumnIndex("servicePrice"));
             profile=cursor.getString(cursor.getColumnIndex("profilePicPath"));
                String serviceCurrency=cursor.getString(cursor.getColumnIndex("serviceCurrency"));
                String serviceAvailable=cursor.getString(cursor.getColumnIndex("serviceAvailable"));
                String serviceName=cursor.getString(cursor.getColumnIndex("serviceName"));
                getDeviceId=cursor.getString(cursor.getColumnIndex("deviceid"));


                numCustomer.setText(""+Experience);
                displayProviderName.setText(""+providerName);
                displayProviderAvailableTime.setText(""+Availability);
                providerDescText.setText(""+providerDesc);
                displayContactNumber.setText(""+providerContact);
                totalProducts.setText("" + count(getDeviceId));
                productTextName.setText(""+serviceName);
                productAvailCityText.setText(""+serviceAvailable);
                Picasso.with(this).load(getService).resize(screenWIdth,200).into(image);


                Picasso.with(Fragment_DisplayItem.this).load(profile).resize(45, 45).into(actionProfile);


                itemPrice.setText(serviceCurrency + "  " + getPrice);
            }while (cursor.moveToNext());
        }
    }
    public String  count(String  i)
    {
        String sel="SELECT * FROM DisplayApi WHERE deviceid='"+i+"'";
        Cursor curs=database.rawQuery(sel,null);
        long j   =curs.getCount();
        i=String.valueOf(j);


        return i;
    }
    }
