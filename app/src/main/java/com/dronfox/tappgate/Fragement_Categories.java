package com.dronfox.tappgate;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Pardeep on 8/30/2015.
 */
public class Fragement_Categories extends Fragment
{


    SimpleCursorAdapter simpleCursorAdapter;
    String[] subCategoryArray={"Plumber","Electrician","Dog Care","Dog Trainer","Carpanter","Laundry"};

    String[] categoriesOnDash={"Engineer","Mechanic","Medical","Food","Teacher","Real Estate","Electonics","Fashion","Vehicals","Home Appliances","Fitness","Music","Counslers","Photography","Decoratore"};
    GridLayoutManager gridLayoutManager;
    RecyclerView recyclerView;
    ActionBar actionbar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_categories,container,false);


        actionbar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionbar.setTitle("Category");
        setHasOptionsMenu(true);

        recyclerView=(RecyclerView)view.findViewById(R.id.allCategoriesRecycler);
        gridLayoutManager=new GridLayoutManager(getActivity(),2);
recyclerView.setLayoutManager(gridLayoutManager);
recyclerView.setAdapter(new BaseAdapter_Categories(getActivity(), categoriesOnDash));
        final String[] from = new String[] {"cityName"};
        final int[] to = new int[] {android.R.id.text1};
simpleCursorAdapter=new SimpleCursorAdapter(getActivity(),android.R.layout.simple_list_item_1,null,from,to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

     view.setOnKeyListener(new View.OnKeyListener() {
         @Override
         public boolean onKey(View v, int keyCode, KeyEvent event) {
             if(keyCode==KeyEvent.KEYCODE_BACK)
             {
                 getActivity().getSupportFragmentManager().popBackStackImmediate();
             }

             return false;
         }
     });

        return view;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    menu.clear();

inflater.inflate(R.menu.searchcategoriesfrag, menu);

        final SearchView search=(SearchView)menu.findItem(R.id.menu_search).getActionView();
        search.setQueryHint("what are you looking For?");
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        search.setSuggestionsAdapter(simpleCursorAdapter);
        search.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {

                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {


                Cursor c = ((SimpleCursorAdapter)search.getSuggestionsAdapter()).getCursor();
                Toast.makeText(getActivity(),""+ c.getString(c.getColumnIndex("cityName")),Toast.LENGTH_LONG).show();


Intent newIntent=new Intent(getActivity(),SearchActivity.class);
                newIntent.putExtra("searchword",c.getString(c.getColumnIndex("cityName")));
                startActivity(newIntent);
                return false;
            }
        });
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

//                Intent iop=new Intent(getActivity(),SearchActivity.class);

                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText)

            {
            populateAdapter(newText);
                return false;
            }
        });
                super.onCreateOptionsMenu(menu, inflater);
    }


    private void populateAdapter(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
        for (int i=0; i<subCategoryArray.length; i++) {
            if (subCategoryArray[i].toLowerCase().startsWith(query.toLowerCase()))
                c.addRow(new Object[] {i, subCategoryArray[i]});
        }
        simpleCursorAdapter.changeCursor(c);
    }


}
