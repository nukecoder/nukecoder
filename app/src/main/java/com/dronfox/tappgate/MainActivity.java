package com.dronfox.tappgate;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String ProjectId="328578885859";
    public static String Authority="com.dronfox.SynAdapterFile";

    String getDeviceId;
    String getPhoneNumber;
    String getCity;
    String getState;
    String getCountry;
    String getUserId;
    String getGcmId;
    String getLocation;

    public static SharedPreferences sharedPreferences;
SQLiteDatabase database;
    public static String AccountType="com.syncFiles.AccountHandle";
    public static  String AccountName="Omiindo Account";
RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;
GoogleCloudMessaging googleCloudMessaging=null;
TelephonyManager telephonyManager;
    EditText edt_ContactDetails;
    Button btnSubmit;
GPSTracker objGpsTracker;
    NetWorkConnectivity objNetworkconnectivity;    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_main);
        objGpsTracker=new GPSTracker(this);
        objNetworkconnectivity=new NetWorkConnectivity(this);
        if(objGpsTracker.canGetLocation()==false)
        {
        ShowDialog("Location is Off","We found that your location is not on, Please Turn on your location",R.drawable.nogps);
        }
        else if(objNetworkconnectivity.isConnectingToInternet()==false)
        {
            ShowDialog("No Internet Connection","Internet Connection is Not Activate,Please Turn on your internet connection",R.drawable.cloud);
        }

        else
        {

            database =openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);


            AccountManager manager=(AccountManager)getSystemService(ACCOUNT_SERVICE);
            Account account=new Account(AccountName,AccountType);
            manager.addAccountExplicitly(account, null, null);

            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            ContentResolver.requestSync(account,Authority,bundle);

            sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
boolean b=sharedPreferences.getBoolean("registered",false);
            telephonyManager=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);
            getDeviceId=telephonyManager.getDeviceId();
            getPhoneNumber=telephonyManager.getLine1Number();




            googleCloudMessaging=GoogleCloudMessaging.getInstance(this);
            edt_ContactDetails = (EditText) findViewById(R.id.fName);
            btnSubmit = (Button) findViewById(R.id.submit_area);
            btnSubmit.setOnClickListener(this);
            edt_ContactDetails.setText("+" + GetCountryZipCode() + "" + getPhoneNumber);
            restAdapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com/").build();
            retrofitInterface=restAdapter.create(RetrofitInterface.class);
            restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
new AsyncFetchGcm().execute();
            new AsyncFetchLocation().execute();





        }

    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.submit_area:

if(edt_ContactDetails.getText().toString().equals(""))
{
    edt_ContactDetails.setError("Please Enter Contact Number");
    Toast.makeText(this,"No Text Found",Toast.LENGTH_LONG).show();
}
                else
{
    getUserId=getDeviceId+""+edt_ContactDetails.getText().toString().replace("+","");

    final ProgressDialog progressDialog=new ProgressDialog(this);
    progressDialog.setMessage("Please Wait, Signing Up");
    progressDialog.show();
    retrofitInterface.insertData(edt_ContactDetails.getText().toString(), getDeviceId, getCity, getState, getUserId, getCountry, getGcmId, new Callback<String>() {
        @Override
        public void success(String s, Response response) {


            progressDialog.dismiss();
        Toast.makeText(MainActivity.this,""+s,Toast.LENGTH_LONG).show();
            Log.e("---",""+s);
         // String get = new String(((TypedByteArray) response.getBody()).getBytes());
            final Dialog sucussfull = new Dialog(MainActivity.this);


            sucussfull.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            sucussfull.setContentView(R.layout.succussfullsignupdialog);
            final TextView succussText = (TextView) sucussfull.findViewById(R.id.textSuccuss);
            succussText.setText("" + s.replace("\"", ""));
            Button process=(Button)sucussfull.findViewById(R.id.proceed);
            process.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SharedPreferences.Editor editor_CheckBit=sharedPreferences.edit();
                    editor_CheckBit.putBoolean("registered", true);
                    editor_CheckBit.putString("getCity", getCity);
                    editor_CheckBit.putString("getState",getState);
                    editor_CheckBit.putString("getCountry",getCountry);



                    editor_CheckBit.commit();
                    sucussfull.dismiss();



                    Intent intent=new Intent(MainActivity.this,Registration.class);
                    startActivity(intent);






                }
            });
            sucussfull.show();


        }

        @Override
        public void failure(RetrofitError retrofitError) {
            Log.e("---",""+retrofitError.getMessage());
        }
    });
    SharedPreferences.Editor editor=sharedPreferences.edit();
    editor.putString("userid",getUserId);
    editor.commit();
}
                break;
        }
    }

    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";
        TelephonyManager manager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }

        }
        return CountryZipCode;

    }
    public void ShowDialog(String textTitle,String Text,int id) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.noconnectionfounddialog, null);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);


        builder.setView(v);
        TextView textViewTitle = (TextView) v.findViewById(R.id.textTitle);
        TextView textViewDesc = (TextView) v.findViewById(R.id.textDesc);
        textViewTitle.setText(textTitle);
        textViewDesc.setText(Text);
        ImageView img = (ImageView) v.findViewById(R.id.imageView);
        img.setImageResource(id);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });

        android.app.AlertDialog a = builder.create();
        a.show();
        Button positive = a.getButton(DialogInterface.BUTTON_POSITIVE);
        positive.setBackgroundColor(Color.parseColor("#f2205ed1"));

        positive.setTextColor(Color.WHITE);
    }
    public class AsyncFetchGcm extends AsyncTask<Void,Void,Void>
    {
            ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {


            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

try {
getGcmId=googleCloudMessaging.register(ProjectId);

}catch (Exception e)
{
    Log.e("----",""+e);
}

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
        Log.e("GCMID",""+getGcmId);

            progressDialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }
    public class AsyncFetchLocation extends  AsyncTask<Void,Void,Void>
    {
ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
           getLocation=getCurrentLocationViaJSON(objGpsTracker.latitude,objGpsTracker.longitude);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.e("----",""+getLocation);
            progressDialog.dismiss();
            if(getLocation.equals("") || getLocation.length()<4)
            {


                LayoutInflater inflater=(LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
                View view=inflater.inflate(R.layout.dialogiflocationnotfound,null);

                AlertDialog.Builder dig=new AlertDialog.Builder(MainActivity.this);

dig.setView(view);
                    final EditText edt_state=(EditText)view.findViewById(R.id.cityName);
                final EditText edt_city=(EditText)view.findViewById(R.id.stateName);
                final EditText edt_country=(EditText)view.findViewById(R.id.CountryName);
                dig.setTitle("We are Unable to find your location");
                dig.setMessage("Please Enter Your Location Manually");
                dig.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCity=edt_city.getText().toString();
                        getState=edt_state.getText().toString();
                                getCountry=edt_country.getText().toString();
                    }
                });

                dig.show();

            }
            else {
                String[] splitText = getLocation.split(",");
                getCity = splitText[0];
                getState = splitText[1];
                getCountry = splitText[2];
            }
            super.onPostExecute(aVoid);
        }
    }
    public static JSONObject getLocationInfo(double lat, double lng) {
        //if (android.os.Build.VERSION.SDK_INT >9) {
        //StrictMode.ThreadPolicy policy= new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);
        HttpGet httpGet = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?latlng="+ lat + "," + lng + "&sensor=true");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder =new StringBuilder();
        try
        {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char)b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public  String getCurrentLocationViaJSON(double lat, double lng)
    {
        JSONObject jsonObj = getLocationInfo(lat, lng);
        String Address1 = "";
        String Address2 = "";
        String State = "";
        String County = "";
        String PIN= "";
        String currentLocation = "";
        String ho="";
        String sub_locality="";
        String getsubloc1="";
        String getsubloc2="";
        String City="";
        String Country="";

        try {
            String status = jsonObj.getString("status").toString();
            if
                    (status.equalsIgnoreCase("OK"))
            {
                JSONArray Results = jsonObj.getJSONArray("results");

                JSONObject zero = Results.getJSONObject(0);
                JSONArray address_components= zero.getJSONArray("address_components");
                for (int i = 0; i < address_components.length(); i++) {
                    JSONObject zero2 = address_components.getJSONObject(i);
                    String long_name = zero2.getString("long_name");
                    JSONArray mtypes = zero2.getJSONArray("types");
                    for(int q=0;q<mtypes.length();q++)
                    {
                    }
                    String Type = mtypes.getString(0);

                    if (Type.equalsIgnoreCase("street_number"))
                    {
                        Address1 =long_name + " ";
                    }
                    else if (Type.equalsIgnoreCase("route"))
                    {
                        Address1 =Address1 + long_name;
                    }
                    else if (Type.equalsIgnoreCase("sublocality_level_1"))
                    {
                        Address2 =long_name;
                    }
                    else if (Type.equalsIgnoreCase("sublocality_level_2"))
                    {
                        getsubloc2 =long_name;
                    }

                    else if (Type.equalsIgnoreCase("locality")) {
                        // Address2= Address2 + long_name + ",";
                        City = long_name;
                    }
                    else if (Type.equalsIgnoreCase("administrative_area_level_2"))
                    {
                        County = long_name;
                    } else if (Type.equalsIgnoreCase("administrative_area_level_1")) {
                        State = long_name;
                    } else if (Type.equalsIgnoreCase("country")) {
                        Country = long_name;
                    } else if (Type.equalsIgnoreCase("postal_code")) {
                        PIN = long_name;
                    }
                }
                currentLocation =   City + ","+ State + "," + Country;
            }
        } catch (Exception e) {
        }
        return currentLocation;
    }
}
