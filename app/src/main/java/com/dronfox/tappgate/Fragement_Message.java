package com.dronfox.tappgate;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Pardeep on 8/29/2015.
 */
public class Fragement_Message extends Fragment
{
    LinearLayoutManager linearLayoutManager;
    RecyclerView recycler;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View views=inflater.inflate(R.layout.fragement_message,container,false);
        recycler=(RecyclerView)views.findViewById(R.id.chat_Recycler);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

       recycler.setAdapter(new BaseAdapterChat(getActivity()));

        return views;



    }
}
