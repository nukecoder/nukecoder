package com.dronfox.tappgate;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Pardeep on 8/30/2015.
 */
public class BaseAdapter_Categories  extends RecyclerView.Adapter<BaseAdapter_Categories.ViewHolder>{

    String[] categoriesOnDash;


   Context context;
    ArrayList<String> listCategoryImages;

   public BaseAdapter_Categories(Context Ctx,String[] categoriesOnDashArray)
   {
       context=Ctx;
    categoriesOnDash=categoriesOnDashArray;
     }

    @Override
    public BaseAdapter_Categories.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View inflaterview= LayoutInflater.from(parent.getContext()).inflate(R.layout.baseadapter_categories, parent,false);
        ViewHolder vh=new ViewHolder(inflaterview);

        return vh;
    }

    @Override
    public void onBindViewHolder(BaseAdapter_Categories.ViewHolder holder, final int position) {
    holder.imageCatImages.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            Fragment FragementSubCats = new Fragement_SubCategories();
            Bundle bundle = new Bundle();

            bundle.putString("bundleData", categoriesOnDash[position]);

            FragementSubCats.setArguments(bundle);
            FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            tranc.replace(R.id.content_frame, FragementSubCats).addToBackStack(null);
            tranc.commit();


//            FragmentTransaction tranc=((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
//
//            tranc.replace(R.id.content_frame,new Fragement_SubCategories()).addToBackStack(null);
//            tranc.commit();

        }
    });
      //  holder.imageCatImages;
        holder.titleText.setText(""+categoriesOnDash[position]);


    }

    @Override
    public int getItemCount() {
        return categoriesOnDash.length;
    }


    public class ViewHolder extends  RecyclerView.ViewHolder {

       ImageView imageCatImages;
        TextView titleText;
        TextView subtitleText;
        public ViewHolder(View itemView)
        {
            super(itemView);
       imageCatImages=(ImageView)itemView.findViewById(R.id.categoryImages);
            titleText=(TextView)itemView.findViewById(R.id.categoryNames);
            subtitleText=(TextView)itemView.findViewById(R.id.categorySubCats);




        }
    }

}
