package com.dronfox.tappgate;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.dronfox.Circular.CircularImageView;
import com.dronfox.LocalDbFile.LocalDb;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class PostService extends Fragment {


    public static boolean check_pic_background = false;

    String getProfilePicPath;
    String getUserId;
    int REQUEST_CODE_CAMERA = 1;
    int REQUEST_CODE_GALLERY = 0;
    SharedPreferences sharedPreferences;
    ArrayAdapter<String> adapterDay;
    ArrayAdapter<String> adapterNight;
    ArrayList<String> listDay;
    ArrayList<String> listNight;
    SharedPreferences PostServiceSharedPreferences;
    String getName;
    String getExperience;
    String getAvailability;
    boolean bln = false;
    ImageView rel;
    Extra_Functions function;
    EditText edtServiceName;
    EditText edtServicePrice;
    Spinner serviceCurrency;
    Spinner availableFor;
    Spinner category;
    Spinner subCategory;
    EditText edtDesc;
    String getServiceName;
    String getServicePrice;
    String getCurrency;
    String getItemAvailability;
    String getCategory;
    String getSubCatgory;
    String getDescription;
    String getImagePath;
    String getPosttimer;
    String getServiceId;
    CircularImageView circularImageView;
    SharedPreferences preferences;


    String getRating = "0";
    String getTotalPost = "0";
    String onlineStatus = "false";
    RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;
    String[] currencyType = {"Rs", "$"};
    ArrayAdapter<String> currencyAdapter;
    String getCity;
    String getState;
    String getCountry;


    String[] AvailFor = {"This Item is Available for", "All", "City Only", "Country Only", "State Only"};
    ArrayAdapter<String> availForAdapter;

    String[] arrayCategory = {"Selct Category", "Mechanic", "Engineer", "Food", "Home Services"};
    String[] arraySubCategoryMechanic = {"Select SubCategory", "Electrician", "Plumber", "Carpanter"};
    String[] arraysubCategoryEngineer = {"Select SubCategory", "Web Development", "Civil Engineer", "Mechanical Engineer"};
    String[] arraysubCategoryFood = {"Select SubCategory", "Chef"};
    String[] arraysubCategoryHomeService = {"Select SubCategory", "Laondry", "Sweeper", "Servent"};

    ArrayAdapter<String> adapterCategory;
    ArrayAdapter<String> adapterSubCategory;
    FloatingActionButton clickPick;
    ActionBar actionbar;
        LocalDb objLocalDb;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        actionbar = ((ActionBarActivity) getActivity()).getSupportActionBar();

        actionbar.setTitle("Post Service");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        View v = inflater.inflate(R.layout.postservice, container, false);
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        getCity=preferences.getString("getCity", "null");
        getState=preferences.getString("getState", "null");
        getCountry=preferences.getString("getCountry","null");

objLocalDb=new LocalDb(getActivity());
        objLocalDb.CreatePostedServiceTable();
        function = new Extra_Functions(getActivity());
        listNight = new ArrayList<String>();
        listDay = new ArrayList<String>();
        PostServiceSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        setHasOptionsMenu(true);

        getUserId=PostServiceSharedPreferences.getString("userid", "null");
        Toast.makeText(getActivity(),getUserId,Toast.LENGTH_LONG).show();
        restAdapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com").build();
        retrofitInterface=restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK)
                {
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }

                return false;
            }
        });

        bln = PostServiceSharedPreferences.getBoolean("digStatus", false);

        MainFunction(v);


        return v;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId())
    {

        case android.R.id.home:
           getActivity().getSupportFragmentManager().popBackStackImmediate();

            break;

        case R.id.tickDone:

            getServiceName=edtServiceName.getText().toString();
            getServicePrice=edtServicePrice.getText().toString();
            getDescription=edtDesc.getText().toString();
            getServiceId=new SimpleDateFormat("ddmmyy_HHmm").format(new Date());
            getServiceId=getServiceId.replace("_", "");
            getPosttimer=new SimpleDateFormat("HH:mm").format(new Date());
            getPosttimer.replace(":","");

            getImagePath="http://omiindo.com/Omiindo/tappgate/images/"+function.imageFileName;



objLocalDb.insertInPostedService(getServiceName,getServicePrice,getCurrency,getItemAvailability,getCategory,getSubCatgory,getDescription,getPosttimer,getUserId,getServiceId,function.filepath);
            retrofitInterface.addItems(getServiceName, getServicePrice, getCurrency, getItemAvailability, getCategory, getSubCatgory, getDescription, getImagePath, getPosttimer, getUserId, getServiceId, new Callback<String>() {
                @Override
                public void success(String s, Response response) {

                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });

//            retrofitInterface.insertMyService(getUserId, getImagePath, getDescription, getServicePrice, getServiceName, getItemAvailability, new Callback<String>() {
//                @Override
//                public void success(String s, Response response) {
//
//                }
//
//                @Override
//                public void failure(RetrofitError retrofitError) {
//
//                }
//            });



                edtServiceName.setText("");
                edtServicePrice.setText("");
                edtDesc.setText("");
            category.setSelection(0);
            subCategory.setSelection(0);
            serviceCurrency.setSelection(0);
            availableFor.setSelection(0);
            try {
                rel.setBackgroundResource(0);
            }catch (Exception e)
            {

            }



            break;
    }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.postserviceclass,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
//
//
//
//
    public void MainFunction(View view) {

        currencyAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, currencyType);
        availForAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, AvailFor);
        adapterCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrayCategory);


        edtServiceName = (EditText) view.findViewById(R.id.serviceName);
        edtServicePrice = (EditText) view.findViewById(R.id.servicePrice);
        serviceCurrency = (Spinner) view.findViewById(R.id.serviceCurrency);
        serviceCurrency.setAdapter(currencyAdapter);
        availableFor = (Spinner) view.findViewById(R.id.availableFor);
        availableFor.setAdapter(availForAdapter);
        clickPick = (FloatingActionButton) view.findViewById(R.id.button_addc);
        category = (Spinner) view.findViewById(R.id.category);
        category.setAdapter(adapterCategory);
        subCategory = (Spinner) view.findViewById(R.id.subCategory);
        edtDesc = (EditText) view.findViewById(R.id.serviceDescription);
        rel = (ImageView) view.findViewById(R.id.megarel);

        serviceCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getCurrency = currencyType[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        availableFor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 1:

                        getItemAvailability="all";

                        break;
                    case 2:
                        getItemAvailability=getCity;
                        break;
                    case 3:
                        getItemAvailability=getCountry;
                        break;
                    case 4:
                        getItemAvailability=getState;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setAdapterFunction(position);
                if (position == 0) {

                } else {
                    getCategory = arrayCategory[position];
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getSubCatgory = adapterSubCategory.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        clickPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_pic_background=false;
                CaptureImageFunction();
            }
        });
    }
    public void setAdapterFunction(int pos) {

        switch (pos) {
            case 1:
                adapterSubCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arraySubCategoryMechanic);
                break;
            case 2:

                adapterSubCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arraysubCategoryEngineer);
                break;
            case 3:

                adapterSubCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arraysubCategoryFood);
                break;
            case 4:
                adapterSubCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arraysubCategoryHomeService);
                break;
        }
        subCategory.setAdapter(adapterSubCategory);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_CODE_CAMERA && data!=null) {

          try {
              Bitmap clicked_image = (Bitmap) data.getExtras().get("data");

              Drawable d = new BitmapDrawable(clicked_image);


              if(check_pic_background==true)
              {
                  circularImageView.setImageBitmap(clicked_image);
              }
              else if(check_pic_background==false) {
                  rel.setBackgroundDrawable(d);
              }
              function.Save_Image_To_sdCard(clicked_image);
              String getImagePathonDisk = function.filepath;
              File f = new File(getImagePathonDisk);
              TypedFile typeFile = new TypedFile("application/octate-stream", f);

              retrofitInterface.insertImage(typeFile, new Callback<String>() {
                  @Override
                  public void success(String s, Response response) {

                  }

                  @Override
                  public void failure(RetrofitError retrofitError) {

                  }
              });
          }catch (Exception e)
          {
              Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_LONG).show();
          }

        } else if (requestCode == REQUEST_CODE_GALLERY && data!=null) {
          try {

              Uri photoUri = data.getData();
              if (photoUri != null) {
                  try {
                      String[] filePathColumn = {MediaStore.Images.Media.DATA};
                      Cursor cursor = getActivity().getContentResolver().query(photoUri, filePathColumn, null, null, null);
                      cursor.moveToFirst();
                      int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                      String filePath = cursor.getString(columnIndex);
                      cursor.close();

                      Bitmap photo = BitmapFactory.decodeFile(filePath);
                      Drawable d = new BitmapDrawable(photo);
                      if(check_pic_background==true)
                      {
                          circularImageView.setImageBitmap(photo);
                      }
                      else if(check_pic_background==false) {
                          rel.setBackgroundDrawable(d);
                      }
                           function.Save_Image_To_sdCard(photo);

                      String getImagePathonDisk = function.filepath;
                      File f = new File(getImagePathonDisk);
                      TypedFile typeFile = new TypedFile("application/octate-stream", f);

                      retrofitInterface.insertImage(typeFile, new Callback<String>() {
                          @Override
                          public void success(String s, Response response) {

                          }

                          @Override
                          public void failure(RetrofitError retrofitError) {

                          }
                      });

                  } catch (Exception e)
                  {
                  Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_LONG).show();
                  }
              }
          }catch (Exception e)
          {
              Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_LONG).show();
          }
        }
    }






    public void CaptureImageFunction()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Product Image");
        builder.setMessage("Please Select a way to pick the Product Image");
        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    Intent gallery = new Intent(Intent.ACTION_PICK);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, REQUEST_CODE_GALLERY);

                } catch (Exception e) {

                }
            }
        });

        builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera, REQUEST_CODE_CAMERA);
                } catch (Exception e) {

                }
            }
        });

        builder.create();
        builder.show();
    }

//
//
//
//
//
//    }
//
//
//
}