package com.dronfox.tappgate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Extra_Functions 
{
public static String  filepath;	
public static String imageFileName;

File sdImageMainDirectory;
Context ctx;
public Extra_Functions(Context c)
{
	// TODO Auto-generated constructor stub
ctx=c;
}



public void Save_Image_To_sdCard(Bitmap photo)
{
	OutputStream fOut = null;

	try {
		// creating folder Photo Editor in gallery to store photos after
		// adding effects
		File root = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/TechWar");
		root.mkdirs();

		// file name generation
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	 imageFileName = timeStamp + ".jpeg";

		// creating new file in the Photo Editor Folder
		File sdImageMainDirectory = new File(root, imageFileName);

		Toast.makeText(ctx, "" + sdImageMainDirectory, Toast.LENGTH_LONG).show();

		Log.e(getClass().getSimpleName(), "sdImageMainDirectory"+ sdImageMainDirectory);
		
		filepath=sdImageMainDirectory.toString();
		
		Toast.makeText(ctx, "original path  "+filepath, Toast.LENGTH_LONG).show();
		
		
		fOut = new FileOutputStream(sdImageMainDirectory);
	} catch (Exception e) {
		Toast.makeText(ctx, "Error occured. Please try again later.",
				Toast.LENGTH_SHORT).show();
	}

	// compressing image and then sending and saving in gallery
	try {
	photo.compress(Bitmap.CompressFormat.JPEG,10 , fOut);

	fOut.flush();
	
	}catch(Exception e)
	{
		Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();
	}

}




}
