package com.dronfox.tappgate;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dronfox.SyncAdapterFiles.ContentProviderClass;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 8/31/2015.
 */
public class Fragement_SubCategories  extends Fragment
{

    String[] suggestionCatregories={"Architect","Developer","Plumber","Electrician","Carpanter"};


    SQLiteDatabase db;
    RecyclerView recyclerViewOnline;
    SharedPreferences preferences;
    LinearLayoutManager linearLayoutManager;
    String getCity;
    String getState;
    String getCountry;
    String getCat;
    ImageView nodatafound;
    TextView nodatatext;

    MainActivity mainActivity;
    ArrayList<String> listName;
    ArrayList<String> listServiceName;
    ArrayList<String> listServiceImagePath;
    ArrayList<String> listRating;
    ArrayList<String> listTimer;
    ArrayList<String> listDesc;
    ArrayList<String> listserviceId;
ArrayList<String> listSubCat;
ActionBar actionbar;
    SQLiteDatabase database;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        actionbar=((ActionBarActivity)getActivity()).getSupportActionBar();

        View view=inflater.inflate(R.layout.fragementsubcategories, container, false);

        database=getActivity().openOrCreateDatabase("omiindoDb",Context.MODE_PRIVATE,null);
        String insertQuery = "CREATE TABLE IF NOT EXISTS DisplayApi(Name,Contactnum,deviceid,Experience,Availability,Rating,Totalpost,profilePicPath,onlineStatus,serviceName,servicePrice,serviceCurrency,serviceAvailable,serviceCategory,serviceSubCategory,serviceDesc,servicePicPath,servicePostTimer,serviceId,gcmId)";

        database.execSQL(insertQuery);

        mainActivity=new MainActivity();
        nodatafound=(ImageView)view.findViewById(R.id.nodatafound);
        nodatatext=(TextView)view.findViewById(R.id.nodataText);
        preferences=mainActivity.sharedPreferences;
        preferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        listName=new ArrayList<String>();
        listServiceName=new ArrayList<String>();
        listServiceImagePath=new ArrayList<String>();
        listRating=new ArrayList<String>();
        listTimer=new ArrayList<String>();
        listSubCat=new ArrayList<String>();
        listDesc=new ArrayList<String>();
        listserviceId=new ArrayList<String>();



        getCat=getArguments().getString("bundleData");

        actionbar.setTitle(""+getCat);
        Toast.makeText(getActivity(),""+getCat,Toast.LENGTH_LONG).show();
        getCity=preferences.getString("getCity", "null");
        getState=preferences.getString("getState", "null");
        getCountry=preferences.getString("getCountry","null");
        setHasOptionsMenu(true);
        Snackbar.make(getActivity().findViewById(android.R.id.content), "Showing Result From " + getCity, Snackbar.LENGTH_LONG).show();
//
        recyclerViewOnline=(RecyclerView)view.findViewById(R.id.recyclerSubCategories);
        recyclerViewOnline.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//
        //SelectQueryFunction(getCat);
SelectQueryFunctionConditionCity(getCat,getCity);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }

                return false;
            }
        });
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_subcategories, menu);
        final SearchView search=(SearchView)menu.findItem(R.id.menu_search).getActionView();
        search.setQueryHint("what are you looking For?");
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        search.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                return false;
            }
        });
    search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {


            return false;
        }

        @Override
        public boolean onQueryTextChange(String d) {
            return true;
        }
    });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

switch (item.getItemId())

{
    case R.id.fromcity:
SelectQueryFunctionConditionCity(getCat,getCity);
        break;
    case R.id.fromcountry:
        SelectQueryFunctionConditionCity(getCat,getCountry);
        break;
    case R.id.fromstate:

        SelectQueryFunctionConditionCity(getCat,getState);
        break;
    case R.id.alloverworld:

        SelectQueryFunction(getCat);
        break;
}

        return super.onOptionsItemSelected(item);
    }










    public void SelectQueryFunction(String Category)
    {
        listName.clear();
        listServiceName.clear();
        listServiceImagePath.clear();
        listRating.clear();
        listTimer.clear();
        listDesc.clear();
        listSubCat.clear();
        listserviceId.clear();
        listSubCat.clear();
String select="SELECT * FROM DisplayApi WHERE serviceCategory='"+Category+"'";
      Cursor cursor=database.rawQuery(select,null);

        if(cursor.getCount() < 1)
        {
nodatafound.setVisibility(View.VISIBLE);
            nodatatext.setVisibility(View.VISIBLE);
            recyclerViewOnline.setVisibility(View.GONE);
        }
        else {


            nodatafound.setVisibility(View.GONE);
            nodatatext.setVisibility(View.GONE);

            recyclerViewOnline.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "" + cursor.getCount(), Toast.LENGTH_LONG).show();


            cursor.moveToFirst();
            do {


                String fetchName = cursor.getString(cursor.getColumnIndex("Name"));
                String fetchServiceName = cursor.getString(cursor.getColumnIndex("serviceName"));
                String fetchservicePicPath = cursor.getString(cursor.getColumnIndex("servicePicPath"));
                String fetchRating = cursor.getString(cursor.getColumnIndex("Rating"));
                String fetTimer = cursor.getString(cursor.getColumnIndex("servicePostTimer"));
                String fetchDesc = cursor.getString(cursor.getColumnIndex("serviceDesc"));
                String fetchServiceId = cursor.getString(cursor.getColumnIndex("serviceId"));
                String getSubCat=cursor.getString(cursor.getColumnIndex("serviceSubCategory"));
                Log.e("-----------", "" + fetchName + "," + fetchServiceName);
                listName.add(fetchName);
                listServiceName.add(fetchServiceName);
                listServiceImagePath.add(fetchservicePicPath);
                listRating.add(fetchRating);
                listTimer.add(fetTimer);
                listDesc.add(fetchDesc);
                listserviceId.add(fetchServiceId);
listSubCat.add(getSubCat);


            }
            while (cursor.moveToNext());


            recyclerViewOnline.setAdapter(new BaseAdapter_SubCategories(getActivity(), listName, listServiceName, listServiceImagePath, listRating, listTimer, listDesc, listserviceId,listSubCat));
        }
        }

    public void SelectQueryFunctionConditionCity(String Category,String Loaction) {
        listName.clear();
        listServiceName.clear();
        listServiceImagePath.clear();
        listRating.clear();
        listTimer.clear();
        listDesc.clear();
        listserviceId.clear();
        listSubCat.clear();
        String select = "SELECT * FROM DisplayApi WHERE serviceCategory='" + Category + "' and serviceAvailable='" + Loaction + "'";
        Cursor cursor = database.rawQuery(select, null);

        if (cursor.getCount() < 1) {
            nodatafound.setVisibility(View.VISIBLE);
            nodatatext.setVisibility(View.VISIBLE);
            recyclerViewOnline.setVisibility(View.GONE);
        } else {


            nodatafound.setVisibility(View.GONE);
            nodatatext.setVisibility(View.GONE);

            recyclerViewOnline.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "" + cursor.getCount(), Toast.LENGTH_LONG).show();


            cursor.moveToFirst();
            do {


                String fetchName = cursor.getString(cursor.getColumnIndex("Name"));
                String fetchServiceName = cursor.getString(cursor.getColumnIndex("serviceName"));
                String fetchservicePicPath = cursor.getString(cursor.getColumnIndex("servicePicPath"));
                String fetchRating = cursor.getString(cursor.getColumnIndex("Rating"));
                String fetTimer = cursor.getString(cursor.getColumnIndex("servicePostTimer"));
                String fetchDesc = cursor.getString(cursor.getColumnIndex("serviceDesc"));
                String fetchServiceId = cursor.getString(cursor.getColumnIndex("serviceId"));
                String getSubCat=cursor.getString(cursor.getColumnIndex("serviceSubCategory"));

                Log.e("-----------", "" + fetchName + "," + fetchServiceName);
                listName.add(fetchName);
                listServiceName.add(fetchServiceName);
                listServiceImagePath.add(fetchservicePicPath);
                listRating.add(fetchRating);
                listTimer.add(fetTimer);
                listDesc.add(fetchDesc);
                listserviceId.add(fetchServiceId);
                listSubCat.add(getSubCat);

            }
            while (cursor.moveToNext());


            recyclerViewOnline.setAdapter(new BaseAdapter_SubCategories(getActivity(), listName, listServiceName, listServiceImagePath, listRating, listTimer, listDesc, listserviceId,listSubCat));
        }


    }

    public void FetchDataOnline()
    {

        final ProgressDialog dailog=new ProgressDialog(getActivity());
        dailog.setMessage("PLease Wait");
        dailog.show();

        RestAdapter adapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com").build();
        RetrofitInterface retrofitInterface=adapter.create(RetrofitInterface.class);
        retrofitInterface.fetchProductDetails(new Callback<List<getterSetterClass>>() {
            @Override
            public void success(List<getterSetterClass> getterSetterClasses, Response response) {
                for(int i=0;i<getterSetterClasses.size();i++)
                {
                    String Name=getterSetterClasses.get(i).getName();
                    String Experience=getterSetterClasses.get(i).getExperience();
                    String Availability=getterSetterClasses.get(i).getAvailability();
                    String Rating=getterSetterClasses.get(i).getRating();
                    String Totalpost=getterSetterClasses.get(i).getTotalpost();
                    String profilePicPath=getterSetterClasses.get(i).getProfilePicPath();
                    String onlineStatus=getterSetterClasses.get(i).getOnlineStatus();
                    String serviceName=getterSetterClasses.get(i).getServiceName();
                    String servicePrice=getterSetterClasses.get(i).getServicePrice();
                    String serviceCurrency=getterSetterClasses.get(i).getServiceCurrency();
                    String serviceAvailable=getterSetterClasses.get(i).getServiceAvailable();
                    String serviceCategory=getterSetterClasses.get(i).getServiceCategory();
                    String serviceSubCategory=getterSetterClasses.get(i).getServiceSubCategory();
                    String serviceDesc=getterSetterClasses.get(i).getServiceDesc();
                    String servicePicPath=getterSetterClasses.get(i).getServicePicPath();
                    String servicePostTimer=getterSetterClasses.get(i).getServicePostTimer();
                    String serviceId=getterSetterClasses.get(i).getServiceId();
                    String gcmId=getterSetterClasses.get(i).getGcmId();



                    listName.add(Name);
                    listServiceName.add(serviceName);
                    listServiceImagePath.add(servicePicPath);
                    listRating.add(Rating);
                    listTimer.add(servicePostTimer);
                    listDesc.add(serviceDesc);
                    listserviceId.add(serviceId);



                    Log.e("dfdsfds",""+Name+","+Experience+","+Availability+","+serviceName+","+servicePrice);

//                    ContentValues contentValues = new ContentValues();
//                    contentValues.put("Name",Name);
//                    contentValues.put("Experience",Experience);
//                    contentValues.put("Availability",Availability);
//                    contentValues.put("Rating",Rating);
//                    contentValues.put("Totalpost",Totalpost);
//                    contentValues.put("profilePicPath",profilePicPath);
//                    contentValues.put("onlineStatus",onlineStatus);
//                    contentValues.put("serviceName",serviceName);
//                    contentValues.put("servicePrice",servicePrice);
//                    contentValues.put("serviceCurrency",serviceCurrency);
//                    contentValues.put("serviceAvailable",serviceAvailable);
//                    contentValues.put("serviceCategory",serviceCategory);
//                    contentValues.put("serviceSubCategory",serviceSubCategory);
//                    contentValues.put("serviceDesc",serviceDesc);
//                    contentValues.put("servicePicPath",servicePicPath);
//                    contentValues.put("servicePostTimer",servicePostTimer);
//                    contentValues.put("serviceId",serviceId);


                    //Uri uri = getActivity().getContentResolver().insert(ContentProviderClass.uri, contentValues);

                    dailog.dismiss();
                }
                recyclerViewOnline.setAdapter(new BaseAdapter_SubCategories(getActivity(), listName, listServiceName, listServiceImagePath, listRating, listTimer, listDesc, listserviceId,listSubCat));

            }

            @Override
            public void failure(RetrofitError retrofitError) {
Log.e("--------------",""+retrofitError.getMessage());
            }
        });
    }


}
