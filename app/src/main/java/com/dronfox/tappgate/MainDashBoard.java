package com.dronfox.tappgate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dronfox.Circular.CircularImageView;
import com.dronfox.LocalDbFile.LocalDb;

import java.util.ArrayList;

/**
 * Created by Pardeep on 8/28/2015.
 */
public class MainDashBoard extends ActionBarActivity //implements AdapterView.OnItemClickListener { ActionBarDrawerToggle actionBarDrawerToggle;

{

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ListView listview;
    ImageView imgEdit;
    ActionBarDrawerToggle actionBarDrawerToggle;
    FragmentTransaction trans;
    NavigationView navigationView;
    CircularImageView circularImageView;
    SharedPreferences preferencesProfile;
    Registration objRegistration;
    LocalDb objLocalDb;

    TextView textView;
    ArrayList<String> listName;
    ArrayList<String> listAvail;
    ArrayList<String> listExper;
    ArrayList<String> listprofilepic;
ArrayList<String> listUserid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 setContentView(R.layout.maindashboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        objRegistration=new Registration();
Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();
        objLocalDb=new LocalDb(this);

        objLocalDb.CreateLocalInfoTable();;






        listName=new ArrayList<String>();
        listAvail=new ArrayList<String>();
        listExper=new ArrayList<String>();
        listprofilepic=new ArrayList<String>();
        listUserid=new ArrayList<String>();


        preferencesProfile=objRegistration.PostServiceSharedPreferences;
        preferencesProfile= PreferenceManager.getDefaultSharedPreferences(this);
        navigationView=(NavigationView)findViewById(R.id.navigation);

        textView =(TextView)navigationView.findViewById(R.id.yexyHeader);
        circularImageView=(CircularImageView)navigationView.findViewById(R.id.profilePicImage);
        objLocalDb.SelectPersonalInfo(listName, listExper, listAvail, listprofilepic, listUserid);
        Bitmap bm= BitmapFactory.decodeFile(listprofilepic.get(0));



        circularImageView.setImageBitmap(bm);
        textView.setText("" + listName.get(0));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        trans=getSupportFragmentManager().beginTransaction();

        switch (menuItem.getItemId())
        {

            case R.id.homebutton:
                trans.replace(R.id.content_frame, new Fragement_Home());
                break;


            case R.id.membership:
                trans.replace(R.id.content_frame, new Fragement_Premium());

                break;
            case R.id.message:
                trans.replace(R.id.content_frame, new Fragement_Message());

                break;
            case R.id.menusettings:
                trans.replace(R.id.content_frame, new Fragement_Settings());

                break;
            case R.id.rate:

                break;
        }
        trans.commit();
        drawerLayout.closeDrawers();

        return false;
    }
});

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
            }

            public void onDrawerClosed(View view) {
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        trans = getSupportFragmentManager().beginTransaction();
        trans.replace(R.id.content_frame, new Fragement_Home());
        trans.commit();

        imgEdit = (ImageView) findViewById(R.id.edit);

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile = new Intent(MainDashBoard.this, ProfileActivity.class);
                startActivity(profile);
                drawerLayout.closeDrawers();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

        objLocalDb.SelectPersonalInfo(listName, listExper, listAvail, listprofilepic, listUserid);
        Bitmap bm= BitmapFactory.decodeFile(listprofilepic.get(0));



        circularImageView.setImageBitmap(bm);
        textView.setText("" + listName.get(0));



    }
}
