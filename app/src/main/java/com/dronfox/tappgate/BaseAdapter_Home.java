package com.dronfox.tappgate;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.wearable.internal.ChannelSendFileResponse;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class BaseAdapter_Home extends RecyclerView.Adapter<BaseAdapter_Home.ViewHolder>
{


    int count;
String[] categoriesOnDash={"","","","Engineer","Mechanic","Medical","Food","Teacher","Real Estate"};
    String[] getCategoriesOnDashImages={"","","","http://omiindo.com/Omiindo/tappgate/iconimages/engineer.jpg","http://omiindo.com/Omiindo/tappgate/iconimages/mechanic.jpg","http://omiindo.com/Omiindo/tappgate/iconimages/medical.jpg","http://omiindo.com/Omiindo/tappgate/iconimages/food.jpg","http://omiindo.com/Omiindo/tappgate/iconimages/teaching.jpg","http://omiindo.com/Omiindo/tappgate/iconimages/realestate.jpg"};

    RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;
    Context context;
    CountDownTimer countDownTimer;
    ArrayList<String> categoriesName;
//    ArrayList<Bitmap> categoriesImage;
//ArrayList<Bitmap> arrayList;
    int viewCount;
    public BaseAdapter_Home(Context c) {
        // TODO Auto-generated constructor stub

        context=c;

//arrayList=bitmaps;
//        categoriesImage=categoryImage;
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
     public    TextView postDialy;
        Button btnPost;
        ViewPager viewPager;
        RelativeLayout mainCategoryLayout;
        CardView cardPager;
        CardView cardPost;
        RelativeLayout card_three;
        CardView card_Four;
        RelativeLayout card_five;
        CardView card_SIX;
        TextView mostViewed;
        RelativeLayout card_seven;
        TextView CatsSeeMore;
        ImageView catImages;
        TextView catTexts;

        public ViewHolder(View itemView) {
            super(itemView);
            // TODO Auto-generated constructor stub
            viewPager=(ViewPager)itemView.findViewById(R.id.viewPager);
            cardPager=(CardView)itemView.findViewById(R.id.card_one);
            cardPost=(CardView)itemView.findViewById(R.id.card_two);
            card_three=(RelativeLayout)itemView.findViewById(R.id.card_three);
card_Four=(CardView)itemView.findViewById(R.id.card_four);
       card_five=(RelativeLayout)itemView.findViewById(R.id.card_five);
       card_SIX=(CardView)itemView.findViewById(R.id.card_SIX);
        mostViewed=(TextView)itemView.findViewById(R.id.MostViewed);
            mainCategoryLayout=(RelativeLayout)itemView.findViewById(R.id.mainCategoryLayout);
       card_seven=(RelativeLayout)itemView.findViewById(R.id.card_seven);
        btnPost=(Button)itemView.findViewById(R.id.postButton);
            CatsSeeMore=(TextView)itemView.findViewById(R.id.Catsseemore);
            catImages=(ImageView)itemView.findViewById(R.id.catImages);
            catTexts=(TextView)itemView.findViewById(R.id.catTxt);
            postDialy=(TextView)itemView.findViewById(R.id.postDialy);

        }

    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return 17;
    }



    @Override
    public void onBindViewHolder(final ViewHolder arg0, final int arg1) {
        // TODO Auto-generated method stub

        if(arg1>2 && arg1<9)
        {
            viewCount=arg1;
        }


if(arg1==0) {
    arg0.cardPager.setVisibility(View.VISIBLE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
else if(arg1==1) {
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.VISIBLE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
 else if(arg1==2) {
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.VISIBLE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
   else if(arg1<9) {
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.VISIBLE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
        else if(arg1==9)
{
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.VISIBLE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
        else if(arg1>9 && arg1<16)
{
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.VISIBLE);
    arg0.card_seven.setVisibility(View.GONE);
}

        else
{
    arg0.cardPager.setVisibility(View.GONE);
    arg0.cardPost.setVisibility(View.GONE);
    arg0.card_three.setVisibility(View.GONE);
    arg0.card_Four.setVisibility(View.GONE);
    arg0.card_five.setVisibility(View.GONE);
    arg0.card_SIX.setVisibility(View.GONE);
    arg0.card_seven.setVisibility(View.GONE);
}
arg0.catTexts.getBackground().setAlpha(150);
arg0.catTexts.setText(categoriesOnDash[viewCount]);
        restAdapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com").build();
        retrofitInterface=restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
        retrofitInterface.getTotalPost(new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                arg0.postDialy.setText("" + s);

            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });


 arg0.mainCategoryLayout.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {


         Fragment FragementSubCats = new Fragement_SubCategories();
         Bundle bundle = new Bundle();
         bundle.putString("bundleData", categoriesOnDash[arg1]);
         FragementSubCats.setArguments(bundle);
         FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
         tranc.replace(R.id.content_frame, FragementSubCats).addToBackStack(null);
         tranc.commit();
     }
 });



arg0.CatsSeeMore.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        tranc.replace(R.id.content_frame, new Fragement_Categories()).addToBackStack(null);
        tranc.commit();
    }
});




//arg0.viewPager.setAdapter(new ViewPagerAdapter(context, arrayList));

       arg0.btnPost.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();

               tranc.replace(R.id.content_frame, new PostService()).addToBackStack(null);
               tranc.commit();

           }

       });
     //  arg0.categoryImage.setImageBitmap(categoriesImage.get(arg1));
try {
    Picasso.with(context).load(getCategoriesOnDashImages[viewCount]).into(arg0.catImages);
}catch (Exception e)
{

}
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        // TODO Auto-generated method stub

        View inflaterview=LayoutInflater.from(arg0.getContext()).inflate(R.layout.baseadapter_home, arg0,false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }



}