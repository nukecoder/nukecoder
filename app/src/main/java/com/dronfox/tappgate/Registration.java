package com.dronfox.tappgate;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.dronfox.Circular.CircularImageView;
import com.dronfox.LocalDbFile.LocalDb;

import java.io.File;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Pardeep on 9/18/2015.
 */
public class Registration extends Activity

{
    String getAvailability;
int REQUEST_CODE_GALLERY=0;
int REQUEST_CODE_CAMERA=1;
ArrayList<String> listDay;
    ArrayList<String>listNight;
    ArrayAdapter<String> adapterDay;
    ArrayAdapter<String> adapterNight;
    EditText edtName;
    CheckBox anytime;
    EditText edtexeperience;
   CircularImageView circularImageView;
    CheckBox specificTime;
    Spinner spinDayTime;
    Spinner spinNightTime;
    Button done;
    String onlineStatus="false";
    TableRow tableRow;
    Snackbar snackbar;
    Extra_Functions functions;
String getTotalPost="0";
    String getRating = "0";
    RetrofitInterface retrofitInterface;
     public static SharedPreferences PostServiceSharedPreferences;
     String getUserId;
    RestAdapter restAdapter;

    SQLiteDatabase database;
TextInputLayout f_userName;
    TextInputLayout     f_experience;
    LocalDb objLocalDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.registration);


objLocalDb=new LocalDb(this);
        objLocalDb.CreateLocalInfoTable();

        functions=new Extra_Functions(this);
        PostServiceSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        getUserId = PostServiceSharedPreferences.getString("userid", "null");
        Toast.makeText(this, "" + getUserId, Toast.LENGTH_SHORT).show();
        listDay = new ArrayList<String>();
        listNight = new ArrayList<String>();
        restAdapter=new RestAdapter.Builder().setEndpoint("http://omiindo.com").build();
        retrofitInterface=restAdapter.create(RetrofitInterface.class);
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);

        for (int i = 0; i < 12; i++) {
            listDay.add("" + i + " Am");
            listNight.add("" + i + " Pm");
        }
        adapterDay = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listDay);

        adapterNight = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listNight);
        tableRow = (TableRow) findViewById(R.id.table_DoubleSeek);
        tableRow.setVisibility(View.GONE);
        edtName = (EditText) findViewById(R.id.userName);
        f_userName=(TextInputLayout)findViewById(R.id.f_userName);
        f_experience=(TextInputLayout)findViewById(R.id.f_experience);
        edtexeperience = (EditText) findViewById(R.id.experience);
        circularImageView = (CircularImageView) findViewById(R.id.profile_image);
        anytime = (CheckBox) findViewById(R.id.anytime);
        specificTime = (CheckBox) findViewById(R.id.specifictimer);
        spinDayTime = (Spinner) findViewById(R.id.time_Day);
        spinNightTime = (Spinner) findViewById(R.id.time_night);

        done = (Button) findViewById(R.id.btnDone);


        circularImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CaptureImageFunction();
            }
        });


        spinDayTime.setAdapter(adapterDay);
        spinNightTime.setAdapter(adapterNight);
        spinDayTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getAvailability = listDay.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinNightTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getAvailability = getAvailability + "-" + listDay.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        anytime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (anytime.isChecked()) {
                    specificTime.setChecked(false);
                    tableRow.setVisibility(View.GONE);
                    getAvailability = "Always";
                } else {
                    specificTime.setChecked(true);
                }
            }
        });

        specificTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (specificTime.isChecked()) {
                    tableRow.setVisibility(View.VISIBLE);
                    anytime.setChecked(false);
                } else {
                    tableRow.setVisibility(View.VISIBLE);
                    anytime.setChecked(true);
                }

            }
        });



        done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        String getName = edtName.getText().toString();
                                       String getExperience = edtexeperience.getText().toString();

                                       String getProfilePicPath = "http://omiindo.com/Omiindo/tappgate/images/" + functions.imageFileName;

                                        if(getName.equals(""))
                                        {
                                            edtName.setError("Please Enter Your Name");
                                        }
                                        else if(getExperience.equals(""))
                                        {
                                            edtexeperience.setError("Please Enter your experience");
                                        }
                                        else if(anytime.isChecked()==false && specificTime.isChecked()==false)
                                        {

                                            Snackbar.make(findViewById(android.R.id.content), "Please Choose Your Availability", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();


                                        }


                                        else {


                                            objLocalDb.INsertIntoLocalInfo(getName, getExperience, getAvailability, functions.filepath, getUserId);
                                            retrofitInterface.insertUserInfo(getName, getExperience, getAvailability, getRating, getTotalPost, getUserId, onlineStatus, getProfilePicPath,
                                                    new Callback<String>() {
                                                        @Override
                                                        public void success(String s, Response response) {

                                                        }

                                                        @Override
                                                        public void failure(RetrofitError retrofitError) {

                                                        }
                                                    });


                                            Log.e("tag", "" + getName + " - " + getExperience + " - " + getAvailability);
                                            SharedPreferences.Editor editor = PostServiceSharedPreferences.edit();
                                            editor.putBoolean("digStatus", true);
                                            editor.commit();

                                            Toast.makeText(Registration.this, "Final File" + functions.filepath, Toast.LENGTH_SHORT).show();

                                            SharedPreferences.Editor imageEdit = PostServiceSharedPreferences.edit();
                                            imageEdit.putString("profileImage", "" + functions.filepath);
                                            imageEdit.commit();


                                            SharedPreferences.Editor imageName = PostServiceSharedPreferences.edit();
                                            imageEdit.putString("profileText", "" + getName);
                                            imageEdit.commit();


                                            Intent iop = new Intent(Registration.this, MainDashBoard.class);
                                            startActivity(iop);
                                            finish();

                                        }
//
//            dig.dismiss();


        }
    });

    }

    public void CaptureImageFunction()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Product Image");
        builder.setMessage("Please Select a way to pick the Product Image");
        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    Intent gallery = new Intent(Intent.ACTION_PICK);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, REQUEST_CODE_GALLERY);

                } catch (Exception e) {

                }
            }
        });

        builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera, REQUEST_CODE_CAMERA);
                } catch (Exception e) {

                }
            }
        });

        builder.create();
        builder.show();
    }









    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == REQUEST_CODE_CAMERA && data!=null) {

            try {
                Bitmap clicked_image = (Bitmap) data.getExtras().get("data");

                Drawable d = new BitmapDrawable(clicked_image);



                    circularImageView.setImageBitmap(clicked_image);
                functions.Save_Image_To_sdCard(clicked_image);
                String getImagePathonDisk = functions.filepath;


                Toast.makeText(Registration.this,"iamge path"+getImagePathonDisk,Toast.LENGTH_SHORT).show();
                File f = new File(getImagePathonDisk);
                TypedFile typeFile = new TypedFile("application/octate-stream", f);

                retrofitInterface.insertImage(typeFile, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });
            }catch (Exception e)
            {
                Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == REQUEST_CODE_GALLERY && data!=null) {
            try {

                Uri photoUri = data.getData();
                if (photoUri != null) {
                    try {
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String filePath = cursor.getString(columnIndex);
                        cursor.close();

                        Bitmap photo = BitmapFactory.decodeFile(filePath);
                        Drawable d = new BitmapDrawable(photo);

                            circularImageView.setImageBitmap(photo);
                        functions.Save_Image_To_sdCard(photo);

                        String getImagePathonDisk = functions.filepath;
                        Toast.makeText(Registration.this,"Image Path  "+getImagePathonDisk,Toast.LENGTH_SHORT).show();
                        File f = new File(getImagePathonDisk);
                        TypedFile typeFile = new TypedFile("application/octate-stream", f);

                        retrofitInterface.insertImage(typeFile, new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                            Intent iop=new Intent(Registration.this,MainDashBoard.class);
                                startActivity(iop);
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });

                    } catch (Exception e)
                    {
                        Toast.makeText(this,""+e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }catch (Exception e)
            {
                Toast.makeText(this,""+e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }







}







