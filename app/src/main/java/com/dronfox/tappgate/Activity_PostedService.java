package com.dronfox.tappgate;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dronfox.LocalDbFile.LocalDb;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/16/2015.
 */
public class Activity_PostedService extends ActionBarActivity
{

    RecyclerView ondemandRecycler;
    LinearLayoutManager linearLayoutManager;
LocalDb objLocalDb;

    ArrayList<String> serviceName;
    ArrayList<String> servicDesc;
    ArrayList<String> serviceAvail;
    ArrayList<String> serviceProfilePic;
    ArrayList<String> servicePrice;
    ArrayList<String> serviceCurrency;
    ArrayList<String> serviceId;
ArrayList<String> serviceUserId;

    ArrayList<String> serviceList1;
    ArrayList<String> serviceList2;
    ArrayList<String> serviceList3;
    ArrayList<String> serviceImage;

    MainActivity mainActivity;
    SharedPreferences preferences;
String getUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       getSupportActionBar().setTitle("Posted Service");
        setContentView(R.layout.activty_myallpostedservices);


        mainActivity=new MainActivity();
        preferences=mainActivity.sharedPreferences;
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        getUserId=preferences.getString("userid", "null");
        Toast.makeText(this,"UserId-"+getUserId,Toast.LENGTH_LONG).show();

        objLocalDb=new LocalDb(this);
        //objLocalDb.CreatePostedServiceTable();

        serviceName=new ArrayList<String>();
        servicDesc=new ArrayList<String>();;
        serviceAvail=new ArrayList<String>();;
        serviceProfilePic=new ArrayList<String>();;
        servicePrice=new ArrayList<String>();;
        serviceCurrency=new ArrayList<String>();;
        serviceId=new ArrayList<String>();;
        serviceUserId=new ArrayList<String>();
        serviceList1=new ArrayList<String>();;

        serviceList2=new ArrayList<String>();;
        serviceList3=new ArrayList<String>();;
try {
    objLocalDb.MyPostedService(serviceName, servicePrice, serviceCurrency, serviceAvail, serviceList1, serviceList2, servicDesc, serviceList3, getUserId, serviceId, serviceProfilePic);
} catch (Exception e)
{

}

        ondemandRecycler = (RecyclerView) findViewById(R.id.ondemandrecycler);
        linearLayoutManager = new LinearLayoutManager(this);
        ondemandRecycler.setLayoutManager(linearLayoutManager);

        // Toast.makeText(this,""+serviceProfilePic.get(0),Toast.LENGTH_LONG).show();
        ondemandRecycler.setAdapter(new BaseAdapter_PostedServices(this, serviceName, servicDesc, serviceAvail, serviceProfilePic, servicePrice, serviceCurrency, serviceId));

    }
}
