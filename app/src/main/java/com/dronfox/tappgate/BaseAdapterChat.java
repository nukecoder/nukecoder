package com.dronfox.tappgate;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseAdapterChat extends RecyclerView.Adapter<BaseAdapterChat.ViewHolder>
{

    Context c;
    public BaseAdapterChat(Context ctx)
    {
        c=ctx;
    }
    @Override
    public BaseAdapterChat.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View inflaterview= LayoutInflater.from(parent.getContext()).inflate(R.layout.chatscreenrecycler, parent,false);
        ViewHolder vh=new ViewHolder(inflaterview);
        return vh;
    }

    @Override
    public void onBindViewHolder(BaseAdapterChat.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 15;
    }

    public class ViewHolder extends  RecyclerView.ViewHolder
    {

        public ViewHolder(View itemView)
        {
            super(itemView);
        }
    }
}