package com.dronfox.tappgate;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Baseadapter_DrawerList extends BaseAdapter
{
    ViewHolder_two obj_view;
    String[] items;
    Integer[] pics;
    Context ctx;

    public Baseadapter_DrawerList(Context c,String[] it,Integer[] pi) {
        // TODO Auto-generated constructor stu
        items=it;
        pics=pi;
        ctx=c;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    public class ViewHolder_two
    {
        ImageView img_titl;
        TextView text_Title;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        if(convertView==null)
        {
            obj_view=new ViewHolder_two();
            LayoutInflater inflater=(LayoutInflater)ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.baseadapter_drawerlist, null);
            obj_view.img_titl=(ImageView)convertView.findViewById(R.id.baseImage);
            obj_view.text_Title=(TextView)convertView.findViewById(R.id.baseText);


            convertView.setTag(obj_view);



        }
        else
        {
            obj_view=(ViewHolder_two)convertView.getTag();
        }
        obj_view.img_titl.setImageResource(pics[position]);
        obj_view.text_Title.setText(items[position]);
        return convertView;
    }




}
