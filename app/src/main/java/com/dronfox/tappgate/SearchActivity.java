package com.dronfox.tappgate;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/13/2015.
 */
public class SearchActivity extends ActionBarActivity {

    SimpleCursorAdapter simpleCursorAdapter;
    String[] subCategoryArray={"Plumber","Electrician","Dog Care","Dog Trainer","Carpanter","Laundry"};

    SQLiteDatabase database;
    ArrayList<String> listName;
    ArrayList<String> listServiceName;
    ArrayList<String> listServiceImagePath;
    ArrayList<String> listRating;
    ArrayList<String> listTimer;
    ArrayList<String> listDesc;
    ArrayList<String> listserviceId;
    ArrayList<String> listSubCat;
SharedPreferences preferences;
    MainActivity mainActivity;
    ImageView nodatafound;
    TextView nodatatext;
    String getCity;
    String getState;
    String getCountry;
    RecyclerView recyclerViewOnline;
String getIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#5F9EA0")));
        setContentView(R.layout.activitysearch);
        getIntent=getIntent().getStringExtra("searchword");
mainActivity=new MainActivity();
        preferences=mainActivity.sharedPreferences;
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        getCity=preferences.getString("getCity", "null");
        getState=preferences.getString("getState", "null");
        getCountry=preferences.getString("getCountry","null");
        recyclerViewOnline=(RecyclerView)findViewById(R.id.my_recycler_view);
        listName=new ArrayList<String>();
        listServiceName=new ArrayList<String>();
        listServiceImagePath=new ArrayList<String>();
listSubCat=new ArrayList<String>();
        listRating=new ArrayList<String>();
        listTimer=new ArrayList<String>();
        listDesc=new ArrayList<String>();
        listserviceId=new ArrayList<String>();
        recyclerViewOnline=(RecyclerView)findViewById(R.id.recyclerSubCategories);
        recyclerViewOnline.setLayoutManager(new LinearLayoutManager(this));
nodatafound=(ImageView)findViewById(R.id.nodatafound);
        nodatatext=(TextView)findViewById(R.id.nodataText);
        final String[] from = new String[] {"cityName"};
        final int[] to = new int[] {android.R.id.text1};
        simpleCursorAdapter=new SimpleCursorAdapter(this,android.R.layout.simple_list_item_1,null,from,to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


        database=openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String insertQuery = "CREATE TABLE IF NOT EXISTS DisplayApi(Name,Contactnum,deviceid,Experience,Availability,Rating,Totalpost,profilePicPath,onlineStatus,serviceName,servicePrice,serviceCurrency,serviceAvailable,serviceCategory,serviceSubCategory,serviceDesc,servicePicPath,servicePostTimer,serviceId,gcmId)";

        database.execSQL(insertQuery);

SelectQueryFunction(getIntent);
        Toast.makeText(this,getIntent,Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_subcategories, menu);
        final SearchView search=(SearchView)menu.findItem(R.id.menu_search).getActionView();
        search.setQueryHint("what are you looking For?");
        SearchManager searchManager = (SearchManager)getSystemService(SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
search.setSuggestionsAdapter(simpleCursorAdapter);
        search.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {

                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {

                return false;
            }
        });
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Cursor c = ((SimpleCursorAdapter)search.getSuggestionsAdapter()).getCursor();
                Toast.makeText(SearchActivity.this,""+ c.getString(c.getColumnIndex("cityName")),Toast.LENGTH_LONG).show();

                SelectQueryFunction(c.getString(c.getColumnIndex("cityName")));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String d)
            {
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

switch (item.getItemId())
{
    case android.R.id.home:
        finish();
        break;

    case R.id.fromcity:
        SelectQueryFunctionConditionCity(getIntent,getCity);
        break;
    case R.id.fromcountry:
        SelectQueryFunctionConditionCity(getIntent,getCountry);
        break;
    case R.id.fromstate:

        SelectQueryFunctionConditionCity(getIntent,getState);
        break;
    case R.id.alloverworld:

        SelectQueryFunction(getIntent);
        break;
}
        return super.onOptionsItemSelected(item);
    }








    public void SelectQueryFunction(String Category) {
        listName.clear();
        listServiceName.clear();
        listServiceImagePath.clear();
        listRating.clear();
        listTimer.clear();
        listDesc.clear();
        listserviceId.clear();
        listSubCat.clear();
        String select = "SELECT * FROM DisplayApi WHERE serviceSubCategory='" + Category + "'";
        Cursor cursor = database.rawQuery(select, null);

        if (cursor.getCount() < 1) {
            nodatafound.setVisibility(View.VISIBLE);
            nodatatext.setVisibility(View.VISIBLE);
            recyclerViewOnline.setVisibility(View.GONE);
        } else {


            nodatafound.setVisibility(View.GONE);
            nodatatext.setVisibility(View.GONE);

            recyclerViewOnline.setVisibility(View.VISIBLE);
            Toast.makeText(this, "" + cursor.getCount(), Toast.LENGTH_LONG).show();


            cursor.moveToFirst();
            do {


                String fetchName = cursor.getString(cursor.getColumnIndex("Name"));
                String fetchServiceName = cursor.getString(cursor.getColumnIndex("serviceName"));
                String fetchservicePicPath = cursor.getString(cursor.getColumnIndex("servicePicPath"));
                String fetchRating = cursor.getString(cursor.getColumnIndex("Rating"));
                String fetTimer = cursor.getString(cursor.getColumnIndex("servicePostTimer"));
                String fetchDesc = cursor.getString(cursor.getColumnIndex("serviceDesc"));
                String fetchServiceId = cursor.getString(cursor.getColumnIndex("serviceId"));
                String getSubCat=cursor.getString(cursor.getColumnIndex("serviceSubCategory"));

                Log.e("-----------", "" + fetchName + "," + fetchServiceName);
                listName.add(fetchName);
                listServiceName.add(fetchServiceName);
                listServiceImagePath.add(fetchservicePicPath);
                listRating.add(fetchRating);
                listTimer.add(fetTimer);
                listDesc.add(fetchDesc);
                listserviceId.add(fetchServiceId);
listSubCat.add(getSubCat);

            }
            while (cursor.moveToNext());


            recyclerViewOnline.setAdapter(new BaseAdapter_SubCategories(this, listName, listServiceName, listServiceImagePath, listRating, listTimer, listDesc, listserviceId,listSubCat));
        }





    }




    public void SelectQueryFunctionConditionCity(String Category,String Loaction) {
        listName.clear();
        listServiceName.clear();
        listServiceImagePath.clear();
        listRating.clear();
        listTimer.clear();
        listDesc.clear();
        listserviceId.clear();
     listSubCat.clear();
        String select = "SELECT * FROM DisplayApi WHERE serviceSubCategory='" + Category + "' and serviceAvailable='" + Loaction + "'";
        Cursor cursor = database.rawQuery(select, null);

        if (cursor.getCount() < 1) {
            nodatafound.setVisibility(View.VISIBLE);
            nodatatext.setVisibility(View.VISIBLE);
            recyclerViewOnline.setVisibility(View.GONE);
        } else {


            nodatafound.setVisibility(View.GONE);
            nodatatext.setVisibility(View.GONE);

            recyclerViewOnline.setVisibility(View.VISIBLE);
            Toast.makeText(this, "" + cursor.getCount(), Toast.LENGTH_LONG).show();


            cursor.moveToFirst();
            do {


                String fetchName = cursor.getString(cursor.getColumnIndex("Name"));
                String fetchServiceName = cursor.getString(cursor.getColumnIndex("serviceName"));
                String fetchservicePicPath = cursor.getString(cursor.getColumnIndex("servicePicPath"));
                String fetchRating = cursor.getString(cursor.getColumnIndex("Rating"));
                String fetTimer = cursor.getString(cursor.getColumnIndex("servicePostTimer"));
                String fetchDesc = cursor.getString(cursor.getColumnIndex("serviceDesc"));
                String fetchServiceId = cursor.getString(cursor.getColumnIndex("serviceId"));
                String fetchSubCate=cursor.getString(cursor.getColumnIndex("serviceSubCategory"));
                Log.e("-----------", "" + fetchName + "," + fetchServiceName);
                listName.add(fetchName);
                listServiceName.add(fetchServiceName);
                listServiceImagePath.add(fetchservicePicPath);
                listRating.add(fetchRating);
                listTimer.add(fetTimer);
                listDesc.add(fetchDesc);
                listserviceId.add(fetchServiceId);
                listSubCat.add(fetchSubCate);
            }
            while (cursor.moveToNext());


            recyclerViewOnline.setAdapter(new BaseAdapter_SubCategories(this, listName, listServiceName, listServiceImagePath, listRating, listTimer, listDesc, listserviceId, listSubCat));
        }
    }

    private void populateAdapter(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
        for (int i=0; i<subCategoryArray.length; i++) {
            if (subCategoryArray[i].toLowerCase().startsWith(query.toLowerCase()))
                c.addRow(new Object[] {i, subCategoryArray[i]});
        }
        simpleCursorAdapter.changeCursor(c);
    }

}





