package com.dronfox.tappgate;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dronfox.DemandAndRegularFiles.BaseAdapter_DemandandRegular;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/22/2015.
 */
public class ShowAllAvailableServices extends ActionBarActivity {
    SQLiteDatabase database;

    ArrayList<String> getNames;
    ArrayList<String> getNumber;

    MainActivity mainActivity;
    SharedPreferences sharedPreferences;
    GridLayoutManager gridLayoutManager;
    RecyclerView ShowAllAvailableServices;
    String getCity;
    public static boolean var = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showallservice);
        ShowAllAvailableServices = (RecyclerView) findViewById(R.id.showallavailableservices);
        getNames = new ArrayList<String>();
        getNumber = new ArrayList<String>();

        mainActivity = new MainActivity();
        sharedPreferences = mainActivity.sharedPreferences;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        getCity = sharedPreferences.getString("getCity", "null");

        database = openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String create = "CREATE TABLE IF NOT EXISTS DisplayApi(userId VARCHAR,Name VARCHAR,Contactnum VARCHAR,deviceid VARCHAR,Experience VARCHAR,Availability VARCHAR,Rating VARCHAR,Totalpost VARCHAR,profilePicPath VARCHAR,onlineStatus VARCHAR,serviceName VARCHAR,servicePrice VARCHAR,serviceCurrency VARCHAR,serviceAvailable VARCHAR,serviceCategory VARCHAR,serviceSubCategory VARCHAR,serviceDesc VARCHAR,servicePicPath VARCHAR,servicePostTimer VARCHAR,serviceId VARCHAR,gcmId)";
        database.execSQL(create);


        gridLayoutManager=new GridLayoutManager(this,3);
        ShowAllAvailableServices.setLayoutManager(gridLayoutManager);



        String select="SELECT  Distinct serviceSubCategory FROM DisplayApi Where serviceAvailable='"+getCity+"'";
        Cursor crs=database.rawQuery(select,null);
        if(crs.getCount() < 1)
        {

        }
        else
        {
            crs.moveToFirst();
            do {

                String getSubCateName=crs.getString(crs.getColumnIndex("serviceSubCategory"));
                String getCount="Select * from DisplayApi where serviceSubCategory='"+getSubCateName+"'";
                Cursor countNumber=database.rawQuery(getCount,null);
                int count= countNumber.getCount();
                getNames.add(getSubCateName);
                        getNumber.add(""+count);

                }while (crs.moveToNext());
            }

        ShowAllAvailableServices.setAdapter(new BaseAdapter_DemandandRegular(this,getNames,getNumber,getCity));

        }

    }
