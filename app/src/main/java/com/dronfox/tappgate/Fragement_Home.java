package com.dronfox.tappgate;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 8/29/2015.
 */
public class Fragement_Home extends Fragment
{
ArrayList<Bitmap> bitmapArrayList;
    ArrayList<Bitmap> bitmapCategories;
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;



    ActionBar actionbar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        actionbar=((ActionBarActivity)getActivity()).getSupportActionBar();
        actionbar.setTitle("Home");
       View view=inflater.inflate(R.layout.fragement_home, container, false);





        bitmapArrayList=new ArrayList<Bitmap>();
        bitmapCategories=new ArrayList<Bitmap>();
        Bitmap b= BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.nogps);
        Bitmap c= BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.cloud);
        Bitmap d= BitmapFactory.decodeResource(getActivity().getResources(),R.mipmap.ic_launcher);
        bitmapArrayList.add(b);
        bitmapArrayList.add(c);
        bitmapArrayList.add(d);


        bitmapCategories.add(b);
        bitmapCategories.add(b);
        bitmapCategories.add(b);
        bitmapCategories.add(b);
        bitmapCategories.add(b);
        bitmapCategories.add(b);
        bitmapCategories.add(b);



        recyclerView=(RecyclerView)view.findViewById(R.id.my_recycler_view);
        gridLayoutManager=new GridLayoutManager(getActivity(),3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
            switch (position)
            {
                case 0:
                    return 3;
                case 1:
                    return 3;
                case 2:
                    return  3;
                case 9:
                    return 3;
                default:
                    return 1;
            }

            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(new BaseAdapter_Home(getActivity()));
        return view;


    }

    @Override
    public void onStart() {
        actionbar.invalidateOptionsMenu();
        super.onStart();
    }
}
