package com.dronfox.DemandAndRegularFiles;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.dronfox.LocalDbFile.LocalDb;
import com.dronfox.tappgate.MainActivity;
import com.dronfox.tappgate.R;
import com.dronfox.tappgate.ShowAllAvailableServices;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/17/2015.
 */
public class OnDemand_ShowMyServices extends ActionBarActivity {


    ArrayList<String> SubCatName;
    ArrayList<String> TotalNum;
    ArrayList<String> ProviderName;
    ArrayList<String> ProviderServiceName;
    ArrayList<String> ProviderPrice;
    ArrayList<String> ProviderCurrency;
    ArrayList<String> ProviderPlace;
    ArrayList<String> ProviderUserId;
    ArrayList<String> ProviderTimer;
    ArrayList<String> ProviderDays;

    RecyclerView showserviceRecyclerView;
    GridLayoutManager grid;

    MainActivity mainActivity;
    SharedPreferences preferences;
    String getUserId;

    LocalDb db;
FloatingActionButton fabButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ondemand_showallservice);


        mainActivity=new MainActivity();
        preferences=mainActivity.sharedPreferences;
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        getUserId=preferences.getString("userid",null);



        showserviceRecyclerView=(RecyclerView)findViewById(R.id.show_allserviceRecycler);
        grid=new GridLayoutManager(this,3);


db=new LocalDb(this);


        SubCatName=new ArrayList<String>();
        TotalNum=new ArrayList<String>();
        ProviderName=new ArrayList<String>();
        ProviderServiceName=new ArrayList<String>();
        ProviderPrice=new ArrayList<String>();
        ProviderCurrency=new ArrayList<String>();
        ProviderPlace=new ArrayList<String>();
        ProviderUserId=new ArrayList<String>();
        ProviderTimer=new ArrayList<String>();
        ProviderDays=new ArrayList<String>();

        db.SelectRegularDemand(getUserId,SubCatName,ProviderServiceName,ProviderName,ProviderPlace,ProviderPrice,ProviderTimer,ProviderDays,ProviderUserId,"demand");

        Log.e("---", "" + ProviderServiceName.size());


showserviceRecyclerView.setLayoutManager(grid);

        showserviceRecyclerView.setAdapter(new BaseAdapter_DemandandRegular(this,ProviderServiceName,ProviderPrice,"Abohar"));













        fabButton=(FloatingActionButton)findViewById(R.id.fabButton);
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showAll=new Intent(OnDemand_ShowMyServices.this,ShowAllAvailableServices.class);
               showAll.putExtra("Extra","true");

                startActivity(showAll);
            }
        });


    }
}
