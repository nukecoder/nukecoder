package com.dronfox.DemandAndRegularFiles;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dronfox.LocalDbFile.LocalDb;
import com.dronfox.tappgate.R;

import java.util.ArrayList;

/**
 * Created by Pardeep on 8/30/2015.
 */
public class BaseAdapter_DemandandRegular  extends RecyclerView.Adapter<BaseAdapter_DemandandRegular.ViewHolder>{



    Activity context;
    ArrayList<String> SubcategoryName;
    ArrayList<String>SubCategoryPersonAvailable;
    String getIntent;
String getCity;
    String getSub;
SQLiteDatabase database;
LocalDb localDb;

    public BaseAdapter_DemandandRegular(Activity activity ,ArrayList<String> subCatName,ArrayList<String> SubCatPerson,String city)
    {
   getCity=city;

        context = activity;
        SubcategoryName=subCatName;
        SubCategoryPersonAvailable=SubCatPerson;
        getIntent=context.getIntent().getStringExtra("Extra");
        Toast.makeText(context,"Intent ="+ getIntent,Toast.LENGTH_LONG).show();



        database = context.openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String create = "CREATE TABLE IF NOT EXISTS DisplayApi(userId VARCHAR,Name VARCHAR,Contactnum VARCHAR,deviceid VARCHAR,Experience VARCHAR,Availability VARCHAR,Rating VARCHAR,Totalpost VARCHAR,profilePicPath VARCHAR,onlineStatus VARCHAR,serviceName VARCHAR,servicePrice VARCHAR,serviceCurrency VARCHAR,serviceAvailable VARCHAR,serviceCategory VARCHAR,serviceSubCategory VARCHAR,serviceDesc VARCHAR,servicePicPath VARCHAR,servicePostTimer VARCHAR,serviceId VARCHAR,gcmId)";
        database.execSQL(create);


    }

    @Override
    public BaseAdapter_DemandandRegular.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View inflaterview= LayoutInflater.from(parent.getContext()).inflate(R.layout.baseadapter_ondemandmyselectedservices, parent,false);
        ViewHolder vh=new ViewHolder(inflaterview);

        return vh;
    }

    @Override
    public void onBindViewHolder(BaseAdapter_DemandandRegular.ViewHolder holder, final int position) {


        holder.DemandNames.setText(""+SubcategoryName.get(position));
        holder.DemandSubCATnum.setText(""+SubCategoryPersonAvailable.get(position));
        holder.card_showService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


if(getIntent.equals("ok"))
{

}

    else if(getIntent.equals("true"))
                {

                    localDb=new LocalDb(context);
                    localDb.CreateRegularDemand();
                    final ArrayList<String> serviceSubCat=new ArrayList<String>();
                    final ArrayList<String> serviceName=new ArrayList<String>();
                    final ArrayList<String> serviceUserId=new ArrayList<String>();
                    final ArrayList<String> serviceuserName=new ArrayList<String>();
                    final ArrayList<String> servicePrice=new ArrayList<String>();
                    final ArrayList<String> serviceCurrency=new ArrayList<String>();
                    final ArrayList<String> serviceAvail=new ArrayList<String>();

                    final Dialog dig=new Dialog(context);
                    dig.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    dig.setContentView(R.layout.bottomsheetdialog);
                    dig.getWindow().setGravity(Gravity.BOTTOM);
                    dig.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dig.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    FloatingActionButton fab=(FloatingActionButton)dig.findViewById(R.id.myFAB);

                    fab.setRippleColor(Color.GREEN);


                    RecyclerView bottomsheetRecycler=(RecyclerView)dig.findViewById(R.id.bottomsheetRecycler);
                    LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
                    bottomsheetRecycler.setLayoutManager(linearLayoutManager);

                    String getData="SELECT serviceSubCategory,userId,Name,serviceName,servicePrice,serviceCurrency,serviceAvailable from DisplayApi Where serviceSubCategory='"+SubcategoryName.get(position)+"' and serviceAvailable='"+getCity+"'";
                    Cursor crsr=database.rawQuery(getData,null);
                    if(crsr.getCount() < 1)
                    {

                    }
                        else {
                        crsr.moveToFirst();
                        do {
                            String subcat=crsr.getString(crsr.getColumnIndex("serviceSubCategory"));
                            String getUserId = crsr.getString(crsr.getColumnIndex("userId"));
                            String getUserName = crsr.getString(crsr.getColumnIndex("Name"));
                            String getServiceName = crsr.getString(crsr.getColumnIndex("serviceName"));
                            String getServicePrice = crsr.getString(crsr.getColumnIndex("servicePrice"));
                            String getServiceCurrency = crsr.getString(crsr.getColumnIndex("serviceCurrency"));

                            String getServiceAvail = crsr.getString(crsr.getColumnIndex("serviceAvailable"));



                            serviceSubCat.add(subcat);
                            serviceUserId.add(getUserId);

                            serviceuserName.add(getUserName);
                            serviceName.add(getServiceName);
                            servicePrice.add(getServicePrice);
                            serviceCurrency.add(getServiceCurrency);
                            serviceAvail.add(getServiceAvail);
                        } while (crsr.moveToNext());


                    }
//
// /
//
//
//
//
                    bottomsheetRecycler.setAdapter(new BaseAdapter_Regular_ServicePeopleList(context, serviceUserId,
                            serviceuserName, serviceName, servicePrice,
                            serviceCurrency, serviceAvail));

fab.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
     for(int i=0;i<serviceName.size();i++)
     {
         localDb.InsertRegularDemand(serviceSubCat.get(i),serviceName.get(i),serviceuserName.get(i),getCity,servicePrice.get(i),null,null,serviceUserId.get(i),"demand");
     }
    }
});


                   dig.show();
               }
                else if(getIntent.equals("false"))
                {
                    Intent settings=new Intent(context,Reguler_Setting.class);
                  settings.putExtra("Category",SubcategoryName.get(position));
                    context.startActivity(settings);

//
//
                }
          }
     });

            }

    @Override
    public int getItemCount() {
        return SubcategoryName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder {

        CardView card_showService;
        ImageView demand_Images;
        TextView DemandNames;
        TextView DemandSubCATnum;
        public ViewHolder(View itemView)
        {
            super(itemView);
            demand_Images=(ImageView)itemView.findViewById(R.id.demand_Images);
            DemandNames=(TextView)itemView.findViewById(R.id.DemandNames);
            DemandSubCATnum=(TextView)itemView.findViewById(R.id.DemandSubCAT);
            card_showService=(CardView)itemView.findViewById(R.id.card_showService);



        }
    }

}
