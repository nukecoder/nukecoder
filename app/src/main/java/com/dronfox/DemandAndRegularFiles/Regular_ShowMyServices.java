package com.dronfox.DemandAndRegularFiles;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dronfox.LocalDbFile.LocalDb;
import com.dronfox.tappgate.MainActivity;
import com.dronfox.tappgate.R;
import com.dronfox.tappgate.ShowAllAvailableServices;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/17/2015.
 */
public class Regular_ShowMyServices extends ActionBarActivity
{

    RecyclerView RegularRecyclerView;
    GridLayoutManager linearLayoutManager;
    FloatingActionButton fabButtons;


    ArrayList<String> SubCatName;
    ArrayList<String> TotalNum;
    ArrayList<String> ProviderName;
    ArrayList<String> ProviderServiceName;
    ArrayList<String> ProviderPrice;
    ArrayList<String> ProviderCurrency;
    ArrayList<String> ProviderPlace;
    ArrayList<String> ProviderUserId;
    ArrayList<String> ProviderTimer;
    ArrayList<String> ProviderDays;
    LocalDb db;
    MainActivity mainActivity;
    SharedPreferences preferences;
    String getUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regular_showmyselectedservices);

        fabButtons=(FloatingActionButton)findViewById(R.id.facbutton);


        RegularRecyclerView=(RecyclerView)findViewById(R.id.regular_RecyclerView);
        linearLayoutManager=new GridLayoutManager(this,3);
        RegularRecyclerView.setLayoutManager(linearLayoutManager);

        db=new LocalDb(this);

        SubCatName=new ArrayList<String>();
        TotalNum=new ArrayList<String>();
        ProviderName=new ArrayList<String>();
        ProviderServiceName=new ArrayList<String>();
        ProviderPrice=new ArrayList<String>();
        ProviderCurrency=new ArrayList<String>();
        ProviderPlace=new ArrayList<String>();
        ProviderUserId=new ArrayList<String>();
        ProviderTimer=new ArrayList<String>();
        ProviderDays=new ArrayList<String>();


        mainActivity=new MainActivity();
        preferences=mainActivity.sharedPreferences;
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        getUserId=preferences.getString("userid",null);

        db.SelectRegularDemand(getUserId,SubCatName,ProviderServiceName,ProviderName,ProviderPlace,ProviderPrice,ProviderTimer,ProviderDays,ProviderUserId,"reguler");


        RegularRecyclerView.setAdapter(new BaseAdapter_DemandandRegular(this, ProviderServiceName, ProviderPrice, "Abohar"));



        fabButtons.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent showAll=new Intent(Regular_ShowMyServices.this,ShowAllAvailableServices.class);
        showAll.putExtra("Extra","false");
        startActivity(showAll);
    }
});


    }
}
