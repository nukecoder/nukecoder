package com.dronfox.DemandAndRegularFiles;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dronfox.LocalDbFile.LocalDb;
import com.dronfox.tappgate.MainActivity;
import com.dronfox.tappgate.R;

import java.util.ArrayList;

/**
 * Created by Pardeep on 9/17/2015.
 */
public class Reguler_Setting extends ActionBarActivity implements View.OnClickListener {
    RecyclerView rec;
    TextView foodTimer;
    int clickSun=0;
    int clickMon=0;
    int clickTue=0;
    int clickWed=0;
    int clickThur=0;
    int clickFri=0;
    int clickSat=0;

LocalDb localDb;

    Button butSun;
    Button butMon;
    Button butTues;
    Button butWed;
    Button butThur;
    Button butFri;
    Button butSat;
SQLiteDatabase database;
FloatingActionButton button_addc;
    ArrayList<String> storeUserId;
    ArrayList<String> storeName;
    ArrayList<String> storeserviceName;
    ArrayList<String> storeservicePrice;
    ArrayList<String> storeservicecurrency;
    ArrayList<String> storeavailable;
    ArrayList<String> storeSubCat;

    MainActivity mainActivity;
    SharedPreferences preferences;
    String getCity;
    String getSubCat;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.regular_settings);
        database = openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String create = "CREATE TABLE IF NOT EXISTS DisplayApi(userId VARCHAR,Name VARCHAR,Contactnum VARCHAR,deviceid VARCHAR,Experience VARCHAR,Availability VARCHAR,Rating VARCHAR,Totalpost VARCHAR,profilePicPath VARCHAR,onlineStatus VARCHAR,serviceName VARCHAR,servicePrice VARCHAR,serviceCurrency VARCHAR,serviceAvailable VARCHAR,serviceCategory VARCHAR,serviceSubCategory VARCHAR,serviceDesc VARCHAR,servicePicPath VARCHAR,servicePostTimer VARCHAR,serviceId VARCHAR,gcmId)";
        database.execSQL(create);

getSubCat=getIntent().getStringExtra("Category");

        localDb=new LocalDb(this);
localDb.CreateOnDemandTable();

        button_addc=(FloatingActionButton)findViewById(R.id.button_addc);
        button_addc.setOnClickListener(this);
        storeUserId=new ArrayList<String>();
        storeName=new ArrayList<String>();
        storeserviceName=new ArrayList<String>();
        storeservicePrice=new ArrayList<String>();
        storeservicecurrency=new ArrayList<String>();
        storeavailable=new ArrayList<String>();
        storeSubCat=new ArrayList<String>();

        preferences=mainActivity.sharedPreferences;

        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        getCity=preferences.getString("getCity","null");
        butSun=(Button)findViewById(R.id.butSun);
        butMon=(Button)findViewById(R.id.butMon);
        butTues=(Button)findViewById(R.id.butTues);
        butThur=(Button)findViewById(R.id.butThur);
        butFri=(Button)findViewById(R.id.butFri);
        butSat=(Button)findViewById(R.id.butSat);
        butWed=(Button)findViewById(R.id.butWed);

foodTimer=(TextView)findViewById(R.id.foodTimer);


        String getData="SELECT serviceSubCategory,userId,Name,serviceName,servicePrice,serviceCurrency,serviceAvailable from DisplayApi Where serviceSubCategory='"+getSubCat+"' and serviceAvailable='"+getCity+"'";
        Cursor crsr=database.rawQuery(getData,null);
        if(crsr.getCount() < 1)
        {

        }
        else
        {
            crsr.moveToFirst();
            do
            {
           String getSub=crsr.getString(crsr.getColumnIndex("serviceSubCategory"));
            String getUserId=crsr.getString(crsr.getColumnIndex("userId"));
                String getUserName=crsr.getString(crsr.getColumnIndex("Name"));
                String getServiceName=crsr.getString(crsr.getColumnIndex("serviceName"));
                String getServicePrice=crsr.getString(crsr.getColumnIndex("servicePrice"));
          String getServiceCurrency=crsr.getString(crsr.getColumnIndex("serviceCurrency"));

                String getServiceAvail=crsr.getString(crsr.getColumnIndex("serviceAvailable"));
          storeUserId.add(getUserId);
                storeSubCat.add(getSub);
                storeName.add(getUserName);
                storeserviceName.add(getServiceName);
                storeservicePrice.add(getServicePrice);
                storeservicecurrency.add(getServiceCurrency);
                storeavailable.add(getServiceAvail);


            }
            while (crsr.moveToNext());
        }

    linearLayoutManager=new LinearLayoutManager(this);
        rec=(RecyclerView)findViewById(R.id.rec);
        rec.setLayoutManager(linearLayoutManager);


        rec.setAdapter(new BaseAdapter_Regular_ServicePeopleList(this, storeUserId,
                        storeName,storeserviceName,storeservicePrice,
                        storeservicecurrency,storeavailable));
rec.setNestedScrollingEnabled(false);

                butSun.setOnClickListener(this);
        butMon.setOnClickListener(this);
        butTues.setOnClickListener(this);
        butWed.setOnClickListener(this);
        butThur.setOnClickListener(this);
        butFri.setOnClickListener(this);
        butSat.setOnClickListener(this);

foodTimer.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        TimePickerDialog dig=new TimePickerDialog(Reguler_Setting.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                foodTimer.setText(""+hourOfDay+":"+minute);

            }
        }, 0, 0, false);

        dig.show();
    }
});



    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.button_addc:

               for(int j=0;j<storeUserId.size();j++)
               {
                   localDb.InsertRegularDemand(storeSubCat.get(j),storeserviceName.get(j),storeName.get(j),storeavailable.get(j),storeservicePrice.get(j),foodTimer.getText().toString(),"null",storeUserId.get(j),"reguler");
               }




                break;
            case R.id.butSun:




                butTues.setOnClickListener(this);
                butWed.setOnClickListener(this);
                butThur.setOnClickListener(this);
                butFri.setOnClickListener(this);
                butSat.setOnClickListener(this);

clickSun++;
function(clickSun,butSun);
                break;

            case R.id.butMon:
                clickMon++;
                function(clickMon,butMon);
                break;

            case R.id.butTues:
                clickTue++;
                function(clickTue,butTues);

                break;

            case R.id.butWed:
                clickWed++;
                function(clickWed,butWed);
                break;

            case R.id.butThur:

              clickThur++;
                function(clickThur,butThur);

                break;

            case R.id.butFri:
                clickFri++;
                function(clickFri,butFri);
                break;

            case R.id.butSat:
               clickSat++;
                function(clickSat,butSat);

                break;



        }
    }

    public void function(int i,Button btn)
    {
if(i%2==0)
{
    btn.setBackgroundResource(R.drawable.roundbuttons);
}
        else
{
    btn.setBackgroundResource(R.drawable.changebuttoncolor);
}
    }
}
