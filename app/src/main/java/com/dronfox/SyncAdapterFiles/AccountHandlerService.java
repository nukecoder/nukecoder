package com.dronfox.SyncAdapterFiles;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Pardeep on 9/4/2015.
 */
public class AccountHandlerService extends Service
{

    AccountHandle objAccountHandle;
    public static String AccountType="com.syncFiles.AccountHandle";
    public static  String AccountName="Omiindo Account";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return objAccountHandle.getIBinder();
    }

    @Override
    public void onCreate() {
        objAccountHandle=new AccountHandle(this);
        super.onCreate();
    }
public static Account getAccount()
{

    return new Account(AccountName,AccountType);
}



    public class AccountHandle extends AbstractAccountAuthenticator
    {
        public AccountHandle(Context context) {

            super(context);
        }

        @Override
        public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
            return null;
        }

        @Override
        public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
            AccountManager manager=(AccountManager)getSystemService(ACCOUNT_SERVICE);
            Account account=new Account(AccountName,AccountType);
            manager.addAccountExplicitly(account,null,null);
            ContentResolver.setSyncAutomatically(account, AccountType, true);


            return null;
        }

        @Override
        public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
            return null;
        }

        @Override
        public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            return null;
        }

        @Override
        public String getAuthTokenLabel(String authTokenType) {
            return null;
        }

        @Override
        public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            return null;
        }

        @Override
        public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
            return null;
        }
    }
}
