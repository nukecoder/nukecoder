package com.dronfox.SyncAdapterFiles;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.dronfox.tappgate.GCMBroadCastReceiver;
import com.dronfox.tappgate.RetrofitInterface;
import com.dronfox.tappgate.getterSetterClass;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 9/4/2015.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {


   RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;
SQLiteDatabase database;
    GCMBroadCastReceiver objReceiver;

    public SyncAdapter(Context context, boolean autoInitialize)
    {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        objReceiver = new GCMBroadCastReceiver();

        Log.e("", "" + objReceiver.checkIfMessageFromServer);
        database = getContext().openOrCreateDatabase("omiindoDb", Context.MODE_PRIVATE, null);
        String create = "CREATE TABLE IF NOT EXISTS DisplayApi(userId VARCHAR,Name VARCHAR,Contactnum VARCHAR,deviceid VARCHAR,Experience VARCHAR,Availability VARCHAR,Rating VARCHAR,Totalpost VARCHAR,profilePicPath VARCHAR,onlineStatus VARCHAR,serviceName VARCHAR,servicePrice VARCHAR,serviceCurrency VARCHAR,serviceAvailable VARCHAR,serviceCategory VARCHAR,serviceSubCategory VARCHAR,serviceDesc VARCHAR,servicePicPath VARCHAR,servicePostTimer VARCHAR,serviceId VARCHAR,gcmId VARCHAR)";
        database.execSQL(create);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://omiindo.com").build();
        RetrofitInterface retrofitInterface = restAdapter.create(RetrofitInterface.class);

        if (objReceiver.checkIfMessageFromServer == false) {
            retrofitInterface.fetchProductDetails(new Callback<List<getterSetterClass>>() {
                @Override
                public void success(List<getterSetterClass> getterSetterClasses, Response response) {
                    for (int i = 0; i < getterSetterClasses.size(); i++) {
                        String Name = getterSetterClasses.get(i).getName();
                        String contact = getterSetterClasses.get(i).getContactnum();
                        String devId = getterSetterClasses.get(i).getDeviceid();
                        String Experience = getterSetterClasses.get(i).getExperience();
                        String Availability = getterSetterClasses.get(i).getAvailability();
                        String Rating = getterSetterClasses.get(i).getRating();
                        String Totalpost = getterSetterClasses.get(i).getTotalpost();
                        String profilePicPath = getterSetterClasses.get(i).getProfilePicPath();
                        String onlineStatus = getterSetterClasses.get(i).getOnlineStatus();
                        String serviceName = getterSetterClasses.get(i).getServiceName();
                        String servicePrice = getterSetterClasses.get(i).getServicePrice();
                        String serviceCurrency = getterSetterClasses.get(i).getServiceCurrency();
                        String serviceAvailable = getterSetterClasses.get(i).getServiceAvailable();
                        String serviceCategory = getterSetterClasses.get(i).getServiceCategory();
                        String serviceSubCategory = getterSetterClasses.get(i).getServiceSubCategory();
                        String serviceDesc = getterSetterClasses.get(i).getServiceDesc();
                        String servicePicPath = getterSetterClasses.get(i).getServicePicPath();
                        String servicePostTimer = getterSetterClasses.get(i).getServicePostTimer();
                        String serviceId = getterSetterClasses.get(i).getServiceId();
                        String gcmId = getterSetterClasses.get(i).getGcmId();
                        String userId=getterSetterClasses.get(i).getUserId();
                        String insertQuery = "INSERT INTO DisplayApi VALUES('"+userId+"','" + Name + "','" + contact + "','" + devId + "','" + Experience + "','" + Availability + "','" + Rating + "','" + Totalpost + "','" + profilePicPath + "','" + onlineStatus + "','" + serviceName + "','" + servicePrice + "','" + serviceCurrency + "','" + serviceAvailable + "','" + serviceCategory + "','" + serviceSubCategory + "','" + serviceDesc + "','" + servicePicPath + "','" + servicePostTimer + "','" + serviceId + "','" + gcmId + "')";
                        database.execSQL(insertQuery);
                        Log.e("Data Entered", ""+serviceName+","+serviceDesc);


                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });


        } else if(objReceiver.checkIfMessageFromServer==true)
        {


            retrofitInterface.fetchProductDetailsCondition(objReceiver.message, new Callback<List<getterSetterClass>>() {
                @Override
                public void success(List<getterSetterClass> getterSetterClasses, Response response) {
                    for (int i = 0; i < getterSetterClasses.size(); i++) {
                        String Name = getterSetterClasses.get(i).getName();
                        String contact = getterSetterClasses.get(i).getContactnum();
                        String devId = getterSetterClasses.get(i).getDeviceid();
                        String Experience = getterSetterClasses.get(i).getExperience();
                        String Availability = getterSetterClasses.get(i).getAvailability();
                        String Rating = getterSetterClasses.get(i).getRating();
                        String Totalpost = getterSetterClasses.get(i).getTotalpost();
                        String profilePicPath = getterSetterClasses.get(i).getProfilePicPath();
                        String onlineStatus = getterSetterClasses.get(i).getOnlineStatus();
                        String serviceName = getterSetterClasses.get(i).getServiceName();
                        String servicePrice = getterSetterClasses.get(i).getServicePrice();
                        String serviceCurrency = getterSetterClasses.get(i).getServiceCurrency();
                        String serviceAvailable = getterSetterClasses.get(i).getServiceAvailable();
                        String serviceCategory = getterSetterClasses.get(i).getServiceCategory();
                        String serviceSubCategory = getterSetterClasses.get(i).getServiceSubCategory();
                        String serviceDesc = getterSetterClasses.get(i).getServiceDesc();
                        String servicePicPath = getterSetterClasses.get(i).getServicePicPath();
                        String servicePostTimer = getterSetterClasses.get(i).getServicePostTimer();
                        String serviceId = getterSetterClasses.get(i).getServiceId();
                        String gcmId = getterSetterClasses.get(i).getGcmId();
                        String insertQuery = "INSERT INTO DisplayApi(Name,Contactnum,deviceid,Experience,Availability,Rating,Totalpost,profilePicPath,onlineStatus,serviceName,servicePrice,serviceCurrency,serviceAvailable,serviceCategory,serviceSubCategory,serviceDesc,servicePicPath,servicePostTimer,serviceId,gcmId)VALUES('" + Name + "','" + contact + "','" + devId + "','" + Experience + "','" + Availability + "','" + Rating + "','" + Totalpost + "','" + profilePicPath + "','" + onlineStatus + "','" + serviceName + "','" + servicePrice + "','" + serviceCurrency + "','" + serviceAvailable + "','" + serviceCategory + "','" + serviceSubCategory + "','" + serviceDesc + "','" + servicePicPath + "','" + servicePostTimer + "','" + serviceId + "','" + gcmId + "')";
                        database.execSQL(insertQuery);


                        Log.e("Data Entered", "" + serviceName);


                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });




        }
    }

}
