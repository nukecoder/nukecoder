package com.dronfox.SyncAdapterFiles;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Objects;

/**
 * Created by Pardeep on 9/4/2015.
 */
public class SyncAdapterService extends Service
{

    SyncAdapter syncAdapter;
    public static  Object object=new Object();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }

    @Override
    public void onCreate()

    {super.onCreate();

        synchronized (object)
        {
if(syncAdapter==null)
{
    syncAdapter=new SyncAdapter(getApplicationContext(),true);
}
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
